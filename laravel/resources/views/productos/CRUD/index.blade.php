@extends('productos.CRUD.layout')

 

@section('content')

    <div class="row">

        <div class="col-lg-12 margin-tb">

            <div class="pull-left">

                <h2>Laravel 8 CRUD</h2>

                <a class="btn btn-primary" href="/"> Back</a>
            </div>

        </div>

    </div>

   

    @if ($message = Session::get('success'))

        <div class="alert alert-success">

            <p>{{ $message }}</p>

        </div>

    @endif

   

    <table class="table table-bordered">

        <tr>

            <th>No</th>

            <th>Nombre</th>

            <th>Descripcion</th>

            <th>Categoria</th>

            <th>Precio</th>

            <th width="280px">Action</th>

        </tr>

        @foreach ($productos as $producto)

        <tr>

            <td>{{ ++$i }}</td>

            <td>{{ $producto->nombre }}</td>

            <td>{{ $producto->descripcion }}</td>
            
            <td>{{ $producto->categoria }}</td>
            
            <td>{{ $producto->precio }}</td>

            <td>

                <form action="{{ route('productos.destroy',$producto->id) }}" method="POST">

   

                    <a class="btn btn-info" href="{{ route('productos.show',$producto->id) }}">Show</a>

    

                    <a class="btn btn-primary" href="{{ route('productos.edit',$producto->id) }}">Edit</a>

   

                    @csrf

                    @method('DELETE')

      

                    <button type="submit" class="btn btn-danger">Delete</button>

                </form>

            </td>

        </tr>

        @endforeach

    </table>
    <a class="btn btn-success" href="{{ route('productos.create') }}"> Create New Producto</a>
  

    {!! $productos->links() !!}

      

@endsection