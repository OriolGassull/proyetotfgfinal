@extends('productos.CRUD.layout')

@section('content')

<div class="row">

    <div class="col-lg-12 margin-tb">

        <div class="pull-left">

            <h2>Añadir un nuevo producto</h2>

        </div>

        <div class="pull-right">

            <a class="btn btn-primary" href="{{ url()->previous() }}"> Volver</a>

        </div>

    </div>

</div><br>

   

@if ($errors->any())

    <div class="alert alert-danger">

        <strong>Error!</strong> Revisa los campos.<br><br>

        <ul>

            @foreach ($errors->all() as $error)

                <li>{{ $error }}</li>

            @endforeach

        </ul>

    </div>

@endif
<form action="{{ route('productos.store') }}" method="POST" role="form" enctype="multipart/form-data">

    {{ csrf_field() }}


    <div class="row">

<div class="col-xs-12 col-sm-12 col-md-12">

    <div class="form-group">

        <strong>Nombre:</strong>

        <input type="text" name="nombre" class="form-control" placeholder="Nombre">

    </div>

</div>

 <div class="col-xs-4 col-sm-4 col-md-4">

    <div class="form-group">

        <strong>Categoria:</strong>

        <select name="categoria" class="form-control" id="categoriasProd">
            <option value="ordenadores">Ordenadores</option>
            <option value="portatiles">Portátiles</option>
            <option value="telefonos">Teléfonos</option>
            <option value="perifericos">Periféricos</option>
            <option value="televisores">Televisores</option>
            <option value="consolas">Consolas</option>
            <option value="impresoras">Impresoras</option>
            <option value="electrodomesticos">Electrodomésticos</option>
        </select>

    </div>

</div>

<div class="col-xs-4 col-sm-4 col-md-4">

    <div class="form-group">

        <strong>Precio:</strong>

        <input class="form-control" type="number" step="any" name="precio" placeholder="Precio">

    </div>

</div>

<div class="col-xs-4 col-sm-4 col-md-4">

    <div class="form-group">

        <strong>Marca:</strong>

        <input type="text" name="marca" class="form-control" placeholder="Marca">

    </div>

</div>

<div class="col-xs-7 col-sm-7 col-md-7">

    <div class="form-group">

        <strong>Descripcion:</strong>

        <textarea class="form-control": name="descripcion" placeholder="Descripcion" style="height:170px"></textarea>

    </div>

</div>


<div class="col-xs-5 col-sm-5 col-md-5" >


        <strong>1 Imagen:</strong>
    
        <input type="file" name="imagen1" class="crearProdImages" >

        <strong>2 Imagen:</strong>

        <input type="file" name="imagen2" class="crearProdImages" accept="image/jpeg, image/png"/>


        <strong>3 Imagen:</strong>

        <input type="file" name="imagen3" class="crearProdImages" accept="image/jpeg, image/png"/>


        <strong>4 Imagen:</strong>

        <input type="file" name="imagen4" class="crearProdImages" accept="image/jpeg, image/png"/>

   
        <strong>5 Imagen:</strong>

        <input type="file" name="imagen5" class="crearProdImages" accept="image/jpeg, image/png"/>

</div>  


</div>

<hr>        
     <!-- Ordenadores-->

     <div class="row" id="ordenadores">
    
    <div class="col-xs-4 col-sm-4 col-md-4"><div class="form-group"><strong>Procesador:</strong><input type="text" name="procesador" class="form-control" placeholder="Procesador"></div></div>

    <div class="col-xs-4 col-sm-4 col-md-4"><div class="form-group"><strong>Tarjeta Grafica:</strong><input type="text" name="tarjetaGrafica" class="form-control" placeholder="Tarjeta Grafica"></div></div>

    <div class="col-xs-2 col-sm-2 col-md-2"><div class="form-group"><strong>Memoria RAM:</strong><input type="text" name="memoria" class="form-control" placeholder="Memoria RAM"></div></div>

    <div class="col-xs-2 col-sm-2 col-md-2"><div class="form-group"><strong>Almacenamiento:</strong><input type="text" name="almacenamiento" class="form-control" placeholder="Almacenamiento"></div></div>

</div>


<!-- Portatiles-->

<div class="row" id="portatiles" style="display:none;">

    <div class="col-xs-4 col-sm-4 col-md-4"><div class="form-group"><strong>Procesador:</strong><input type="text" name="procesador" class="form-control" placeholder="Procesador"></div></div>

    <div class="col-xs-4 col-sm-4 col-md-4"><div class="form-group"><strong>Tarjeta Grafica:</strong><input type="text" name="tarjetaGrafica" class="form-control" placeholder="Tarjeta Grafica"></div></div>

    <div class="col-xs-2 col-sm-2 col-md-2"><div class="form-group"><strong>Memoria RAM:</strong><input type="text" name="memoria" class="form-control" placeholder="Memoria RAM"></div></div>

    <div class="col-xs-2 col-sm-2 col-md-2"><div class="form-group"><strong>Almacenamiento:</strong><input type="text" name="almacenamiento" class="form-control" placeholder="Almacenamiento"></div></div>

    <div class="col-xs-2 col-sm-2 col-md-2"><div class="form-group"><strong>Bateria:</strong><input type="text" name="bateria" class="form-control" placeholder="Bateria"></div></div>

    <div class="col-xs-2 col-sm-2 col-md-2"><div class="form-group"><strong>Pulgadas:</strong><input type="text" name="pulgadas" class="form-control" placeholder="Pulgadas"></div></div>

</div>


<!-- Telefonos-->

<div class="row" id="telefonos" style="display:none;">

    <div class="col-xs-4 col-sm-4 col-md-4"><div class="form-group"><strong>Procesador:</strong><input type="text" name="procesador" class="form-control" placeholder="Procesador"></div></div>

    <div class="col-xs-2 col-sm-2 col-md-2"><div class="form-group"><strong>Memoria RAM:</strong><input type="text" name="memoria" class="form-control" placeholder="Memoria RAM"></div></div>

    <div class="col-xs-2 col-sm-2 col-md-2"><div class="form-group"><strong>Almacenamiento:</strong><input type="text" name="almacenamiento" class="form-control" placeholder="Almacenamiento"></div></div>

    <div class="col-xs-2 col-sm-2 col-md-2"><div class="form-group"><strong>Bateria:</strong><input type="text" name="bateria" class="form-control" placeholder="Bateria"></div></div>

    <div class="col-xs-2 col-sm-2 col-md-2"><div class="form-group"><strong>Pulgadas:</strong><input type="text" name="pulgadas" class="form-control" placeholder="Pulgadas"></div></div>

    <div class="col-xs-2 col-sm-2 col-md-2" style="display:none;"><div class="form-group"><strong>Tipo de pantalla:</strong> <input type="text" name="tipoPantalla" class="form-control" placeholder="Tipo de pantalla"></div></div>

</div>


<!-- Perifericos-->

<div class="row" id="perifericos" style="display:none;">

    <div class="col-xs-2 col-sm-2 col-md-2"><div class="form-group"><strong>Conectividad:</strong><input type="text" name="conectividad" class="form-control" placeholder="Conectividad"></div></div>

</div>


<!-- Televisores-->

<div class="row" id="televisores" style="display:none;">

    <div class="col-xs-2 col-sm-2 col-md-2"><div class="form-group"><strong>Pulgadas:</strong><input type="text" name="pulgadas" class="form-control" placeholder="Pulgadas"></div></div>

    <div class="col-xs-2 col-sm-2 col-md-2"><div class="form-group"><strong>Tipo de pantalla:</strong> <input type="text" name="tipoPantalla" class="form-control" placeholder="Tipo de pantalla"></div></div>

</div>


<!-- Impresoras-->

<div class="row" id="impresoras" style="display:none;">

    <div class="col-xs-2 col-sm-2 col-md-2"><div class="form-group"><strong>Conectividad:</strong><input type="text" name="conectividad" class="form-control" placeholder="Conectividad"></div></div>

    <div class="col-xs-2 col-sm-2 col-md-2"><div class="form-group"><strong>Tipo de impresión:</strong><input type="text" name="tipoImpresion" class="form-control" placeholder="Tipo de impresión"></div></div>

    <div class="col-xs-2 col-sm-2 col-md-2"><div class="form-group"><strong>Altura:</strong><input type="text" name="altura" class="form-control" placeholder="Altura"></div></div>

    <div class="col-xs-2 col-sm-2 col-md-2"><div class="form-group"><strong>Anchura:</strong><input type="text" name="anchura" class="form-control" placeholder="Anchura"></div></div>
    
</div>


<!-- Electrodomesticos-->

 <div class="row" id="electrodomesticos" style="display:none;">

    <div class="col-xs-2 col-sm-2 col-md-2"><div class="form-group"><strong>Altura:</strong><input type="text" name="altura" class="form-control" placeholder="Altura"></div></div>

    <div class="col-xs-2 col-sm-2 col-md-2"><div class="form-group"><strong>Anchura:</strong><input type="text" name="anchura" class="form-control" placeholder="Anchura"></div></div>
    
</div>

<div class="row">

    <div class="col-xs-4 col-sm-4 col-md-4"><div class="form-group"><strong>Otros 1:</strong><input type="text" name="otros1" class="form-control" placeholder="otros 1"></div></div>

    <div class="col-xs-4 col-sm-4 col-md-4"><div class="form-group"><strong>Otros 2:</strong><input type="text" name="otros2" class="form-control" placeholder="otros 2"></div></div>
    
    <div class="col-xs-4 col-sm-4 col-md-4"><div class="form-group"><strong>Otros 3:</strong><input type="text" name="otros3" class="form-control" placeholder="otros 3"></div></div>
 
</div>


    <div class="col-xs-12 col-sm-12 col-md-12 text-center">

            <button type="submit" id="botonSmbit" class="btn btn-primary">Submit</button>

        </div>
</form>


    <script>

        var ordenadores = document.getElementById("ordenadores");   var portatiles = document.getElementById("portatiles");
        var telefonos = document.getElementById("telefonos");   var perifericos = document.getElementById("perifericos");
        var televisores = document.getElementById("televisores");   var impresoras = document.getElementById("impresoras");
        var electrodomesticos = document.getElementById("electrodomesticos");
        
        var pr0 = document.getElementsByName("procesador")[0]; var pr1 = document.getElementsByName("procesador")[1]; var pr2 = document.getElementsByName("procesador")[2]; 
        var tg0 = document.getElementsByName("tarjetaGrafica")[0]; var tg1 = document.getElementsByName("tarjetaGrafica")[1];
        var mm0 = document.getElementsByName("memoria")[0]; var mm1 = document.getElementsByName("memoria")[1]; var mm2 = document.getElementsByName("memoria")[2]; 
        var al0 = document.getElementsByName("almacenamiento")[0]; var al1 = document.getElementsByName("almacenamiento")[1]; var al2 = document.getElementsByName("almacenamiento")[2];
        var bt0 = document.getElementsByName("bateria")[0]; var bt1 = document.getElementsByName("bateria")[1];
        var tp0 = document.getElementsByName("tipoPantalla")[0]; var tp1 = document.getElementsByName("tipoPantalla")[1];
        var pl0 = document.getElementsByName("pulgadas")[0]; var pl1 = document.getElementsByName("pulgadas")[1]; var pl2 = document.getElementsByName("pulgadas")[2];
        var cn0 = document.getElementsByName("conectividad")[0]; var cn1 = document.getElementsByName("conectividad")[1];
        var at0 = document.getElementsByName("altura")[0]; var at1 = document.getElementsByName("altura")[1];
        var an0 = document.getElementsByName("anchura")[0]; var an1 = document.getElementsByName("anchura")[1];

        $("#categoriasProd").change(function() {

            pr0.value = ""; pr1.value = ""; pr2.value = ""; tg0.value = ""; tg1.value = ""; mm0.value = ""; mm1.value = ""; mm2.value = "";
            al0.value = ""; al1.value = ""; al2.value = ""; bt0.value = ""; bt1.value = ""; tp0.value = ""; tp1.value = ""; pl0.value = ""; 
            pl1.value = ""; pl2.value  =""; cn0.value = ""; cn1.value = ""; at0.value = ""; at1.value = ""; an0.value = ""; an1.value = "";
            var categoria = document.getElementById("categoriasProd").value;
            
            if(categoria == "ordenadores"){ ordenadores.style.display = "block"; ocultar("ordenadores"); }

            if(categoria == "portatiles"){ portatiles.style.display = "block"; ocultar("portatiles"); }

            if(categoria == "telefonos"){ telefonos.style.display = "block"; ocultar("telefonos"); }

            if(categoria == "perifericos"){ perifericos.style.display = "block"; ocultar("perifericos"); }

            if(categoria == "televisores"){ televisores.style.display = "block"; ocultar("televisores"); }

            if(categoria == "consolas"){ ocultar("consolas"); }

            if(categoria == "impresoras"){ impresoras.style.display = "block"; ocultar("impresoras"); }

            if(categoria == "electrodomesticos"){ electrodomesticos.style.display = "block"; ocultar("electrodomesticos"); }

                  
        });        

        function ocultar(categoria){

            if(categoria == "consolas" || categoria != null){
                ordenadores.style.display = "none";  portatiles.style.display = "none"; telefonos.style.display = "none"; 
                perifericos.style.display = "none"; televisores.style.display = "none"; impresoras.style.display = "none"; 
                electrodomesticos.style.display = "none";
            }
            
            if(categoria == "ordenadores"){  ordenadores.style.display = "block"; }
            if(categoria == "portatiles"){  portatiles.style.display = "block"; }
            if(categoria == "telefonos"){  telefonos.style.display = "block"; }
            if(categoria == "perifericos"){  perifericos.style.display = "block"; }
            if(categoria == "televisores"){  televisores.style.display = "block"; }
            if(categoria == "impresoras"){  impresoras.style.display = "block"; }
            if(categoria == "electrodomesticos"){  electrodomesticos.style.display = "block"; }
        }
                
                
        $(document).ready(function(){
            $("#botonSmbit").click(function() {

                if(pr0.value != ""){pr1.value = pr0.value; pr2.value = pr0.value;} if(pr1.value != ""){pr0.value = pr1.value; pr2.value = pr1.value;} if(pr2.value != ""){pr0.value = pr2.value; pr1.value = pr2.value;}
                if(mm0.value != ""){mm1.value = mm0.value; mm2.value = mm0.value;} if(mm1.value != ""){mm0.value = mm1.value; mm2.value = mm1.value;} if(mm2.value != ""){mm0.value = mm2.value; mm1.value = mm2.value;}
                if(al0.value != ""){al1.value = al0.value; al2.value = al0.value;} if(al1.value != ""){al0.value = al1.value; al2.value = al1.value;} if(al2.value != ""){al0.value = al2.value; al1.value = al2.value;}
                if(pl0.value != ""){pl1.value = pl0.value; pl2.value = pl0.value;} if(pl1.value != ""){pl0.value = pl1.value; pl2.value = pl1.value;} if(pl2.value != ""){pl0.value = pl2.value; pl1.value = pl2.value;}
                if(cn0.value != ""){cn1.value = cn0.value} if(cn1.value != ""){cn0.value = cn1.value}
                if(at0.value != ""){at1.value = at0.value} if(at1.value != ""){at0.value = at1.value}
                if(an0.value != ""){an1.value = an0.value} if(an1.value != ""){an0.value = an1.value}
                if(tg0.value != ""){tg1.value = tg0.value} if(tg1.value != ""){tg0.value = tg1.value}
                if(bt0.value != ""){bt1.value = bt0.value} if(bt1.value != ""){bt0.value = bt1.value}
            }); 
        });

    </script>

@endsection