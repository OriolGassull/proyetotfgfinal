
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
        integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">    
    <script src='https://kit.fontawesome.com/a076d05399.js'></script>
    <script src="https://unpkg.com/ionicons@5.2.3/dist/ionicons.js"></script>
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" />
    <link rel="stylesheet" href="{{ asset('css/style.css') }}"/>

    <title>Electro Mes</title>
</head>

        <script>
            $(document).ready(function(){
                $("#desplegableCategoriaOff").click(function(){
                    $("#desplegableCategoria").show("slow");
                });
                $("#cerrarDesplegableCat").click(function(){
                    $("#desplegableCategoria").hide("slow");
                });
            });   
            
            $(document).ready(function(){
                $("#lupaINPUT").click(function(){
                    $("#buscarINPUT").show();
                    $(".transFullPage").show(); 
                    $(".input-sm").show();    
                });

                $("#cerrarBuscarINPUT").click(function(){
                    $("#buscarINPUT").hide();
                    $(".transFullPage").hide();
                    $(".input-sm").hide();
                });

            });
        </script>
<body>  
           
            <div class="container-fluid">    
                <div class="row" style="position: fixed; width: 100%; background-color: white; z-index: 5; height: 70px;">

                    <!--Desplegable-->
                    <div class="col-2 col-sm-2 col-md-1 col-lg-1 col-xl-1" id="desplegableCategoriaOff">
                        <svg width="3.5em" height="3.5em" viewBox="0 0 16 16" class="bi bi-list cur mt-2"
                            style="cursor: pointer;" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd"
                                d="M2.5 11.5A.5.5 0 0 1 3 11h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4A.5.5 0 0 1 3 7h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4A.5.5 0 0 1 3 3h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z" />
                        </svg>
                    </div>
                    
                    <div id="desplegableCategoria">
                        <div id="cerrarDesplegableCat"><i class="fa fa-close"></i></div>
                        @guest
                            @if (Route::has('login'))     
                                <h6 id="desplegableUser"><a href="{{ route('register') }}">{{ __('Inicar Sesión') }}</a></h6>
                            @endif

                            @else
                            <h6 id="desplegableUser">Bienvenid@, <a href="/detalles-usuario">{{ Auth::user()->name }}</a></h6>
                        @endguest
                        <hr>        

                        <div class="row ml-1">
                            <i class="material-icons iconoDesplegable col-1 col-sm-1 col-md-1 col-lg-1 col-xl-1">computer</i>
                            <a href="{{ route('categoria.show','ordenadores') }}"><h6 class="categoriasDesplegable col-11 col-sm-11 col-md-11 col-lg-10 col-xl-10">Ordenadores</h6></a>                     
                        </div>

                        <div class="row ml-1">
                            <i class="material-icons iconoDesplegable col-1 col-sm-1 col-md-1 col-lg-1 col-xl-1">laptop</i>
                            <a  href="{{ route('categoria.show','portatiles') }}"><h6 class="categoriasDesplegable col-11 col-sm-11 col-md-11 col-lg-10 col-xl-10">Portátiles</h6></a>                     
                        </div>
                        

                        <div class="row ml-1">
                            <i class="fas fa-mobile-alt iconoDesplegable col-1 col-sm-1 col-md-1 col-lg-1 col-xl-1" style="margin-top:1px; position:relative; left:5px;"></i>
                            <a  href="{{ route('categoria.show','telefonos') }}"><h6 class="categoriasDesplegable col-11 col-sm-11 col-md-11 col-lg-10 col-xl-10">Teléfonos</h6></a>                     
                        </div>

                        <div class="row ml-1">
                            <i class="fa fa-keyboard-o iconoDesplegable col-1 col-sm-1 col-md-1 col-lg-1 col-xl-1" style="margin-top:2px; position:relative; left:3px;"></i>
                            <a  href="{{ route('categoria.show','perifericos') }}"><h6 class="categoriasDesplegable col-11 col-sm-11 col-md-11 col-lg-10 col-xl-10">Periféricos</h6></a>                        
                        </div>

                        <div class="row ml-1">
                            <i class="material-icons iconoDesplegable col-1 col-sm-1 col-md-1 col-lg-1 col-xl-1">tv</i>
                            <a  href="{{ route('categoria.show','televisores') }}"><h6 class="categoriasDesplegable col-11 col-sm-11 col-md-11 col-lg-10 col-xl-10">Televisores</h6></a>
                        </div>

                        <div class="row ml-1">
                            <i class="fa fa-gamepad iconoDesplegable col-1 col-sm-1 col-md-1 col-lg-1 col-xl-1" style="margin-top:3px;"></i>
                            <a href="{{ route('categoria.show','consolas') }}"><h6 class="categoriasDesplegable col-11 col-sm-11 col-md-11 col-lg-10 col-xl-10">Consolas</h6></a>
                        </div>

                        <div class="row ml-1">
                            <i class="material-icons iconoDesplegable col-1 col-sm-1 col-md-1 col-lg-1 col-xl-1">print</i>
                            <a  href="{{ route('categoria.show','impresoras') }}"><h6 class="categoriasDesplegable col-11 col-sm-11 col-md-11 col-lg-10 col-xl-10">Impresoras</h6></a>
                        </div>

                        <div class="row ml-1">
                            <i class="fa fa-hdd-o iconoDesplegable col-1 col-sm-1 col-md-1 col-lg-1 col-xl-1" style="left:3px; position:relative"></i>
                            <a  href="{{ route('categoria.show','electrodomesticos') }}"><h6 class="categoriasDesplegable col-11 col-sm-10 col-md-11 col-lg-10 col-xl-10">Electrodomésticos</h6></a>
                        </div>

                        <hr>

                        <div class="row">   
                            <p style="margin-left:40px;"class="col-sm-10 col-md-10 col-lg-10 col-xl-10">ElectroMes<i class="far fa-copyright" style="font-size:10px; position:relative; bottom:5px; "></i></p>
                        </div>
                    </div>
                <div id="buscarINPUT">
                    <div class="row mt-3">
                        <div class="col-1 col-sm-1 col md-1"></div>
                        <div class="col-9 col-sm-9 md-9"></div>
                    
                        <i id="cerrarBuscarINPUT" class='far fa-window-close col-1 col-sm-1 col md-1'></i>
                    </div>
                </div>