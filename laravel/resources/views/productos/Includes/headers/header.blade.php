                    
           
           <table class="table" id="datatable"></table>

<script>
    var datos = [];
    $(document).ready( function () {
        var table = $('#datatable').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": "{{ route('api.customers.index') }}",
                "dataSrc": function(json){
                    for( var i=0, ien=json.data.length; i < ien ; i++){
                       datos.push(json.data[i]);
                    }
                    
                    return json.data;
                }
            },
            "columns": [
                
                { data: 'nombre', name: 'nombre', render:function(data, type, row){  
                    return "<a href='/productos/"+ row.id +"'><img style='width: 45px; height: 45px;' src='/producto/" + row.categoria + "/" + row.id + "/" + row.imagen1 + "'></a>"
                }},
                { data: 'categoria', name: 'categoria', render:function(data, type, row){
                    return "<div style='white-space: nowrap;overflow: hidden; width:165px'><a href='/productos/"+ row.id +"'>" + row.nombre + "</a></div>"
                }}
            ]
        });
    });
    $(document).ready(function(){
        $(".dataTables_filter label").css("color", "white");
        $("#datatable_length").hide();
        $("#dataTables_filter").hide();
        $("#datatable_info").hide();
        $("#datatable_paginate").hide();
        $("#datatable_paginate").hide();
        $(".sorting_asc").hide();
        $(".sorting").hide();
        $(".dataTable").hide();
    });
    function myFunction(x) {
        if (x.matches) { // If media query matches
            $(document).ready(function(){
                $(".input-sm").hide();
            });
        } else {
            $(document).ready(function(){
                $(".input-sm").show();
            });
        }
    }
    
    var x = window.matchMedia("(max-width: 767px)")
    myFunction(x) // Call listener function at run time
    x.addListener(myFunction) // Attach listener function on state changes

    $(document).ready(function(){
        $(".input-sm").focusin(function(){
            $(".dataTable").show();
        });
        $(".input-sm").focusout(function(){
            $(".dataTable").delay(10).fadeOut();
            
        }); 
    });
</script>

           <div class="transFullPage"></div>  
                    <!-- Buscador -->
                    <div class="col-3 col-sm-3 col-md-3 col-lg-3 col-xl-4 mt-3 d-none d-sm-none d-md-block"></div>

                    <!-- Ofertas XL -->
                    <div style="margin-left:51%; float: left; position: relative; top:-67px;"
                        class="d-none d-sm-none d-md-none d-lg-none d-xl-block">
                        <a href="/ofertas"><div class="row mt-4">
                            <i class='fas fa-bookmark' style='font-size:25px; margin-right:5px;'></i>
                            <h5 style="cursor: pointer;">Ofertas</h5>
                        </div></a>
                    </div>

                    <!-- Ofertas -->
                    <div class="d-block d-sm-block d-md-block d-lg-block d-xl-none mt-3"
                        style="margin-left: 71%; position: relative; bottom:58px;">
                        <a href="/ofertas"><i class='fas fa-bookmark' style='font-size:25px; margin-left: 10px; margin-top: 3px; color:black;'></i></a>
                    </div>

                      @guest

                    <!-- Carrito XL -->
                    <div style="margin-left:62%;  position: relative;top:-123px;"
                        class="d-none d-sm-none d-md-none d-lg-none d-xl-block">
                        <a href=""><div class="row mt-4">
                            <i class="fa fa-shopping-cart" style='font-size:25px; margin-right:5px; position:relative;'></i>    
                            <h5 style="cursor: pointer;" >Mi carrito</h5>
                        </div></a>
                    </div>

                     <!-- Carrito -->
                     <div class="d-block d-sm-block d-md-block d-lg-block d-xl-none mt-3"
                        style="margin-left: 60%;  position: relative; bottom: 106px; cursor:pointer;">
                        <a href=""><i class="fa fa-shopping-cart" style='font-size:25px; margin-left: 10px; margin-top: 3px; color:black;'></i></a>
                    </div>
                    
                @else

                    <!-- Carrito XL -->
                    <div style="margin-left:62%;  position: relative; top:-123px;"
                        class="d-none d-sm-none d-md-none d-lg-none d-xl-block">
                        <a href="{{ route('carrito.show',Auth::user()->id) }}"><div class="row mt-4">
                            <i class="fa fa-shopping-cart" style='font-size:25px; margin-right:5px; position:relative;'></i>    
                            <h5 style="cursor: pointer;" >Mi carrito</h5>
                        </div></a>
                    </div>


                    <!-- Carrito -->
                    <div class="d-block d-sm-block d-md-block d-lg-block d-xl-none mt-3"
                        style="margin-left: 60%;  position: relative; bottom: 106px; cursor:pointer;">
                        <a href="{{ route('carrito.show',Auth::user()->id) }}"><i class="fa fa-shopping-cart" style='font-size:25px; margin-left: 10px; margin-top: 3px; color:black;'></i></a>
                    </div>
                    
                @endguest   



                    <!-- Centro Soporte XL -->
                    <div style="margin-left:74%; float: left; position: relative;  bottom:180px;"
                        class="d-none d-sm-none d-md-none d-lg-none d-xl-block">
                        <a href="/soporte"><div class="row mt-4">
                            <i class='fa fa-question' style='font-size:25px; margin-right:5px;'></i>
                            <h5 style="cursor: pointer;">Soporte</h5>
                        </div></a>
                    </div>


                     <!-- Centro Soporte -->
                     <div class="d-block d-sm-block d-md-block d-lg-block d-xl-none mt-3"
                        style="margin-left: 80%; position: relative; bottom: 155px;">
                        <a href="/soporte"><i class='fa fa-question' style='font-size:25px; margin-left: 10px; margin-top: 3px; color:black;'></i></a>
                    </div>

               


                    <!-- Cuenta de Usuario XL -->
                    <div style="margin-left: 84%; float: right; position: relative; bottom:240px;"
                        class="d-none d-sm-none d-md-none d-lg-none d-xl-block">
                    
                    <!--Auteticaccion-->
                    @guest
                        <div class="row mt-4 ml-1"  >
                            <div class="dropdown" > 
                                <i class='fas fa-user-alt' style='font-size:25px; margin-right:5px; cursor: pointer; '></i>

                                <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">
                                Mi cuenta<span class="caret"></span></button>



                                <ul class="dropdown-menu">
                                    <!--Logearse -->
                                    @if (Route::has('login'))     
                                        <li><a class="dropdown-item" href="{{ route('login') }}">{{ __('Iniciar Sesión') }}</a></li>
                                    @endif
                                    <!--Registrarse -->
                                    @if (Route::has('register'))  
                                        <li><a class="dropdown-item" href="{{ route('register') }}">{{ __('Registrarse') }}</a></li>
                                    @endif
                                </ul>
                            </div>
                        </div>

                        <!-- Usuario Logeado -->
                    @else
                        <div class="row mt-4 ml-1">
                            <div class="dropdown">
                                <i class='fas fa-user-alt' style='font-size:25px; margin-right:5px;'></i>

                                <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">
                                {{ Auth::user()->name }}<span class="caret"></span></button>
                                
                                
                                <ul class="dropdown-menu">
                            
                                    <li><a class="dropdown-item" href="/detalles-usuario">{{ Auth::user()->name }}</li></a>
                                    <li><a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); 
                                        document.getElementById('logout-form').submit();">{{ __('Cerrar Sesión') }}</a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                            @csrf
                                        </form>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    @endguest
                    </div>

                    <!-- Cuenta de Usuario -->
                    <div class="d-block d-sm-block d-md-block d-lg-block d-xl-none mt-3"
                        style="margin-left: 90%; position: relative; bottom:223px;">
                 
                        @guest
                        <div class="row ml-1 mt-4">
                            <div class="dropdown"> 
                                <i class='fas fa-user-alt dropdown-toggle' data-toggle="dropdown" 
                                style='font-size:25px; margin-left: 10px;'><span class="caret"></span></i>

                                <ul class="dropdown-menu">
                                    <!--Logearse -->
                                    @if (Route::has('login'))     
                                        <li><a class="dropdown-item" href="{{ route('login') }}">{{ __('Iniciar Sesión') }}</a></li>
                                    @endif
                                    <!--Registrarse -->
                                    @if (Route::has('register'))  
                                        <li><a class="dropdown-item" href="{{ route('register') }}">{{ __('Registrarse') }}</a></li>
                                    @endif
                                </ul>
                            </div>
                        </div>

                        <!-- Usuario Logeado -->
                    @else
                        <div class="row mt-4 ml-1">
                            <div class="dropdown">
                            <i class='fas fa-user-alt dropdown-toggle' data-toggle="dropdown" 
                                style='font-size:25px; margin-left: 10px;'><span class="caret"></span></i>
                                
                                
                                <ul class="dropdown-menu">
                            
                                    <li><a class="dropdown-item" href="/detalles-usuario">{{ Auth::user()->name }}</li></a>
                                    <li><a class="dropdown-item" href="{{ url('productos') }}" onclick="event.preventDefault(); 
                                        document.getElementById('logout-form').submit();">{{ __('Cerrar Sesión') }}</a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                            @csrf
                                        </form>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    @endguest
                    </div>
                   

                    <!-- Icono lupa-->
                    <div class="d-sm-none d-md-none d-lg-none d-xl-none d-sm-block d-block mt-3"
                        style="margin-left: 51%; position: relative; bottom: 268px;">
                        <i class='fa fa-search' id="lupaINPUT" style='font-size:25px; cursor:pointer;'></i>   
                    </div>
                
                </div> 
        </div>