<footer class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="row">
            <div class="col-2 col-sm-3 col-md-4 col-lg-4 col-xl-5"></div>
            <div class="col-8 col-sm-7 col-md-6 col-lg-4 col-xl-4">
                <a href="/productos" class="fab fa-facebook rds"></a>
                <a href="/productos" class="fab fa-youtube rds"></a>
                <a href="/productos" class="fab fa-instagram rds"></a>
                <a href="/productos" class="fab fa-google rds"></a>
                <a href="/productos" class="fab fa-twitter rds"></a>
                <a href="/productos" class="fab fa-linkedin rds"></a>
            </div>
            <div class="col-2 col-sm-2 col-md-2 col-lg-4 col-xl-3"></div>
        </div>
        <div class="row">
            <div class="col-0 col-sm-0 col-md-1 col-lg-1 col-xl-1"></div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">
                <ul>
                    <li><a href="/soporte" class="enlaceF">Centro de soporte</a></li>
                    <li>|</li>
                    <li><a href="/" class="enlaceF">Aviso legal</a></li>
                    <li>|</li>
                    <li><a href="/" class="enlaceF">Política de cookies</a></li>
                    <li>|</li>
                    <li><a href="/" class="enlaceF">Política de privacidad</a></li>
                    <li>|</li>
                    <li><a href="/" class="enlaceF">Política de contratación</a></li>
                    <li>|</li>
                    <li><a href="/" class="enlaceF">Formas de pago</a></li>
                </ul>
            </div>
            <div class="col-0 col-sm-0 col-md-1 col-lg-1 col-xl-1"></div>
        </div>
        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <p id="gramtek">2020 - Gramtek</p>
        </div>
        <hr style="background-color: black;">
        <p id="infoPag">Electro Mes CIF D63289452. Calle del Mazo 10. Polígono Industrial San José de Valderas, 28923 ,
            Alcorcón, Madrid , ESPAÑA.</p>

    </footer>
    </body>
</html>
