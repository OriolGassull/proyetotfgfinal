@include('productos.Includes.headers.header-links')
<!--Logo Empresa-->         
        <div>
            <a href="/productos"><img src="../LogoEmrpesa.png" 
            style="cursor: pointer; width: 130px; height: 40px; margin-top: 12px; margin-right: 15px;"></a>
        </div>
@include('productos.Includes.headers.header')

<!--Mapa de Navegación-->
        <nav>
            <div class="row">
                <div id="mapaNavegacion" class="col-12 col-sm-7 col-md-5 col-lg-5 col-xl-5"><a href="/productos">Inicio</a> > <a  href="{{ route('categoria.show',$producto->categoria) }}">{{ $producto->categoria }}</a> > {{ $producto->nombre }}</div>
                <div id="textoPagina" class="d-none d-sm-block d-md-block d-lg-block col-sm-5 col-md-7 col-lg-7 col-xl-7"></div>
            </div>
        </nav>
        <script>
            var cantidad = [];
            $(document).ready(function(){
                cantidad[0] = document.getElementById("numeroCantidad").value;
                
                if(cantidad[0] >= 0){
                    $(".chevron:eq(1)").click(function(){
                        cantidad[0]++;
                        $("#numeroCantidad").val(cantidad[0]);
                    });
                }
                if(cantidad[0] >= 0){
                    $(".chevron:eq(0)").click(function(){
                        cantidad[0]--;
                        $("#numeroCantidad").val(cantidad[0]);
                    });
                }
                
            }); 

            $(document).ready(function(){
                $("#carrouselDetalleProd").click(function(){
                    $("#carruselImg1").show();
                    $(".transFullPage").show();
                    $("#img1").show();$("#img2").hide();$("#img3").hide();$("#img4").hide();$("#img5").hide();
                });
                $(".transFullPage").click(function(){
                    $("#carruselImg1").hide();
                    $(".transFullPage").hide();
                });
            });

            $(document).ready(function(){
                $(".rating").click(function(e){

                    valor = e.originalEvent.target.defaultValue;

                    if(e.originalEvent.target.defaultValue == 2 ){

                        valor = 4;

                    }else if(e.originalEvent.target.defaultValue == 4){

                        valor = 2;
                    }

                    document.getElementById("valoracionRes").value = valor;

                });  
            });

            $(document).ready(function(){
                $("#enlaceMediaValoracion").mouseover(function(){
                    $("#mediaValoracion").show();
                });
                $("#enlaceMediaValoracion").mouseout(function(){
                    $("#mediaValoracion").hide();
                });
                
                $("#imgagen2").click(function(){
                    $("#img2").show();$("#img1").hide();$("#img3").hide();$("#img4").hide();$("#img5").hide();
                });
                $("#imgagen3").click(function(){
                    $("#img3").show();$("#img1").hide();$("#img2").hide();$("#img4").hide();$("#img5").hide();
                });
                $("#imgagen4").click(function(){
                    $("#img4").show();$("#img1").hide();$("#img2").hide();$("#img3").hide();$("#img5").hide();
                });
                $("#imgagen5").click(function(){
                    $("#img5").show();$("#img1").hide();$("#img2").hide();$("#img3").hide();$("#img4").hide();
                });
                
            });

        </script>
<body>
       <div class="transFullPage"></div>
    
<div style="padding-top: 110px;"></div>

<div class="row col-sm-12 col-md-12 col-lg-12 col-xl-12">

                    <!--Carrosuel imagenes articulo-->
    <div id="carruselImg1" class="carousel slide col-8 col-sm-8 col-md-7 col-lg-7 col-xl-6" data-ride="carousel">

        <!-- Indicadores -->
        <ul class="carousel-indicators">
            <li data-slide-to="0" class="active"></li>
            <li data-slide-to="1"></li>
            <li data-slide-to="2"></li>
            <li data-slide-to="3"></li>
            <li data-slide-to="4"></li>
        </ul>

        <!-- Imagenes Carrusel -->
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img src="../producto/{{ $producto->categoria }}/{{ $producto->id }}/{{ $producto->imagen1 }}" width="100%" height="auto" onerror="style='display:none;'"/>
            </div>
            @if( $producto->imagen2 != "")
            <div class="carousel-item">
                <img src="../producto/{{ $producto->categoria }}/{{ $producto->id }}/{{ $producto->imagen2 }}" width="100%" height="auto" onerror="style='display:none;'"/>
            </div>
            @endif
            @if( $producto->imagen3 != "")
            <div class="carousel-item">
                <img src="../producto/{{ $producto->categoria }}/{{ $producto->id }}/{{ $producto->imagen3 }}" width="100%" height="auto" onerror="style='display:none;'"/>
            </div>
            @endif
            @if( $producto->imagen4 != "")
            <div class="carousel-item">
                <img  src="../producto/{{ $producto->categoria }}/{{ $producto->id }}/{{ $producto->imagen4 }}" width="100%" height="auto" onerror="style='display:none;'"/>
            </div>
            @endif
            @if( $producto->imagen5 != "")
            <div class="carousel-item">
                <img src="../producto/{{ $producto->categoria }}/{{ $producto->id }}/{{ $producto->imagen5 }}" width="100%" height="auto" onerror="style='display:none;'"/>
            </div>
            @endif
        </div>

            <!-- Izq y derecha controlles -->
            <a class="carousel-control-prev" href="#carruselImg1" data-slide="prev">
                <span class="carousel-control-prev-icon"></span>
            </a>
            <a class="carousel-control-next" href="#carruselImg1" data-slide="next">
                <span class="carousel-control-next-icon"></span>
            </a>
    </div>
    <div class="col-sm-1 col-md-1 col-lg-3 col-xl-4"></div>
    </div>
    <div class="container">
        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="row">
        
            <aside class="col-sm-12 col-md-12 col-lg-5 col-xl-5" >
                <div id="carrouselDetalleProd">
                    <img id="img1" src="../producto/{{ $producto->categoria }}/{{ $producto->id }}/{{ $producto->imagen1 }}">
                    <img id="img2" src="../producto/{{ $producto->categoria }}/{{ $producto->id }}/{{ $producto->imagen2 }}" style="display:none;">
                    <img id="img3" src="../producto/{{ $producto->categoria }}/{{ $producto->id }}/{{ $producto->imagen3 }}" style="display:none;">
                    <img id="img4" src="../producto/{{ $producto->categoria }}/{{ $producto->id }}/{{ $producto->imagen4 }}" style="display:none;">
                    <img id="img5" src="../producto/{{ $producto->categoria }}/{{ $producto->id }}/{{ $producto->imagen5 }}" style="display:none;">
                </div>
                
                <div>
                    <div class="row" style=" border: black 2px solid; position:relative; bottom: 30px;">
                        <div class="cajasImagenes col-3 col-sm-3 col-md-3 col-lg-3 col-xl-3">
                            <img id="imgagen2" src="../producto/{{ $producto->categoria }}/{{ $producto->id }}/{{ $producto->imagen2 }}" onerror="style='display:none;'"/>
                        </div>
                        <div class="cajasImagenes col-3 col-sm-3 col-md-3 col-lg-3 col-xl-3">
                            <img id="imgagen3" src="../producto/{{ $producto->categoria }}/{{ $producto->id }}/{{ $producto->imagen3 }}" onerror="style='display:none;'"/>
                        </div>
                        <div class="cajasImagenes col-3 col-sm-3 col-md-3 col-lg-3 col-xl-3">
                            <img id="imgagen4" src="../producto/{{ $producto->categoria }}/{{ $producto->id }}/{{ $producto->imagen4 }}" onerror="style='display:none;'"/>
                        </div>
                        <div class="cajasImagenes col-3 col-sm-3 col-md-3 col-lg-3 col-xl-3">
                            <img id="imgagen5" src="../producto/{{ $producto->categoria }}/{{ $producto->id }}/{{ $producto->imagen5 }}" onerror="style='display:none;'"/>
                        </div>
                    </div>
                </div>
            </aside>
            
           

            <article  class="col-sm-12 col-md-12 col-lg-7 col-xl-7">

                <h3>{{ $producto->nombre }}</h3>

                <hr>
               
                <!--Valoración producto-->
                <section class="row">
                    @for ($i = 0; $i < $mediaValoracion;$i++)   
                        <i class="fa fa-star rating1"></i>
                    @endfor
                
                
                    <div class="mt-1">
                        |<b id="enlaceMediaValoracion"> Número de valoraciones</b>
                        <b id="mediaValoracion">Total estrellas / número reseñas = {{ $mediaValoracion }}</b>
                    </div>
                </section>

                <hr>
                
                <!--Detalles del producto-->
                <section id="detallesDetalleProd">
                    <div class="row" >
                        <div class="ml-3 col-7 col-sm-7 col-md-7 col-lg-7">
                            <div class="row">
                                <h1 id="precioDetalleProd"><b>{{ $producto->precio }}€</b></h1>
                                <h3 class="mt-2 ml-2 text-muted"><?php $precioSINIVA = $producto->precio; echo $precioSINIVA = $producto->precio - ($producto->precio * 21 / 100)?>€</h3>
                                <h6 class="ml-2 mt-3 text-muted">SIN IVA</h6>
                            </div>   
                        </div>
                        <div class="mt-2 col-12 col-sm-4 col-md-4 col-lg-4">
                    @guest  
                    @else  
                        <!--Si es Administrador-->
                        @if(Auth::user()->esAdmin == 'true')
                            <div class="row">
                                <form action="{{ route('productos.destroy',$producto->id) }}" method="POST">
                                    <a class="btn btn-primary botonCrud" href="{{ route('productos.edit',$producto->id) }}">Editar</a>
                                    @csrf @method('DELETE')
                                    <button type="submit" class="btn btn-primary botonCrud">ELiminar</button>
                                </form>
                            </div>
                        @endif
                    
                    @endguest
                        </div>
                    </div> 
                </section> 
                <section class="ml-3 mt-3">

                    <div class="row mt-2">
                        <b>Marca :</b> 
                        <div class="row ml-2">
                            <p>{{ $producto->marca }}</p>
                        </div>
                    </div>

                    <div class="row mt-2">
                        <b>Vendidos :</b> 
                        <div class="row ml-2">
                            <p>{{ $producto->vendidos }} Unidades</p>
                        </div>
                    </div>  

                    <div class="row mt-2">
                        <b>Disponibilidad :</b> 
                        <div class="row ml-2">
                            <p>¡En stock! Entrega <?php $dia= date("d"); $dia = $dia + 2; echo date($dia."-m-Y")?></p>
                        </div>
                    </div>

            <!--Si NO ha iniciado sesión-->   

        @guest

                    <div class="row">
                        <b class="mt-2">Cantidad :</b> 
                        <div class="row ml-2">
                            <ion-icon name="chevron-back-outline" class="chevron"></ion-icon>
                            <input type="text" id="numeroCantidad"  value="1">     
                            <ion-icon name="chevron-forward-outline" class="chevron"></ion-icon>

                        </div>
                    </div> 
                </section>  
                <section>
                    <a href="{{ route('login') }}"><button class="btn btn-dark" href="{{ route('login') }}" id="botonComprar">COMPRAR</button></a>
                    <a href="{{ route('login') }}"><button class="btn btn-dark" href="{{ route('login') }}" id="botonCarritoProd"> <i class="fa fa-shopping-cart"></i></button></a>
                    <a href="{{ route('login') }}"><button class="btn btn-dark" id="botonCarritoProd"> <i class="fas fa-heart"></i></button></a>
                </section>

        @else
            <section>
                    <div class="row">
                        <b class="mt-2">Cantidad :</b> 
                        <div class="row ml-2">
                            
            <!--Solo pueda agregar una vez el producto en el carrito-->

            @if($id_clienteCarrito ==  Auth::user()->id )
               

                            <ion-icon name="chevron-back-outline" class="chevron"></ion-icon>
                            <input type="text" name="cantidad" id="numeroCantidad" value="1">
                           
                            <ion-icon name="chevron-forward-outline" class="chevron"></ion-icon>
                        </div>
                    </div> 
                </section>
                
                <section>
                    <a href="/carrito/{{Auth::user()->id}}"><button class="btn btn-dark" id="botonComprar">COMPRAR</button></a> 
                    <button type="submit" class="btn btn-dark" id="botonCarritoProd"> 
                        <i class="fa fa-shopping-cart" style=" color: #86c232;"></i>
                    </button>
                    <button class="btn btn-dark" id="botonCarritoProd" ><i class="fas fa-heart"></i></button>
                </section>

                <!--Si el producto NO está en el carrito-->

            @else
                        <!--Meter Producto en la cesta--> 
                   
                                <form action="{{ route('carrito.store') }}" method="POST">
                                    @csrf
                                    <ion-icon name="chevron-back-outline" class="chevron"></ion-icon>
                                    <input type="text" name="cantidad" id="numeroCantidad" value="1">
                                    <ion-icon name="chevron-forward-outline" class="chevron"></ion-icon>
                            
                                    <input type="text" name="id_producto" value="{{$producto->id}}" style="display:none;"> 
                                    <input type="text" name="nombreProducto" value="{{$producto->nombre}}" style="display:none;">
                                    <input type="text" name="precioProducto" value="{{$producto->precio}}" style="display:none;">
                                    <input type="text" name="id_cliente" value="{{Auth::user()->id}}" style="display:none;">     
                            </div>
                        </div> 
                    </section>

                    <section>
                        <a href="/carrito/{{Auth::user()->id}}"><button class="btn btn-dark" id="botonComprar">COMPRAR</button></a> 
                        <button type="submit" class="btn btn-dark" id="botonCarritoProd"> 
                            <i class="fa fa-shopping-cart"></i>
                        </button>
                    </section>
                                </form>
            @endif
<!--*****************************************************************************************************************-->

            <!--Solo pueda agregar una vez el producto en favoritos-->

            @if($id_clienteFavorito ==  Auth::user()->id )   
                    <button class="btn btn-dark" id="botonFavoritoProd"> <i class="fas fa-heart" style=" color: #86c232;"></i></button>
            @else
                    <!--Meter Producto en Favoritos-->
                    
                    <form action="{{ route('favorito.store') }}" method="POST">
                        @csrf
                            
                        <input type="text" name="id_producto" value="{{$producto->id}}" style="display:none;"> 
                        <input type="text" name="nombreProducto" value="{{$producto->nombre}}" style="display:none;">
                        <input type="text" name="precioProducto" value="{{$producto->precio}}" style="display:none;">
                        <input type="text" name="id_cliente" value="{{Auth::user()->id}}" style="display:none;">     
                            
                        <button class="btn btn-dark" type="sumbit" id="botonFavoritoProd"> <i class="fas fa-heart"></i></button>
                
                    </form>

            @endif

<!--*****************************************************************************************************************-->

                

        @endguest
            </article>
        </div>
            <hr>
        <div class="row">
            <h1>Detalles del producto</h1>
            <article id="descDetalleProd" class="col-sm-12 col-md-12 col-lg-10 col-xl-10">
                <p> 
                <b>DESCRIPCIÓN: </b> {{$producto->descripcion }}
                </p>
                <section id="caracteristicasProd">
                    
                        <!--Categoria ordenadores-->

                        @if($producto->categoria == 'ordenadores')
                        
                            <b>Marca: </b> {{$producto->marca }}<br>
                            <b>Procesador: </b> {{$producto->procesador }}<br>
                            <b>Memoria: </b> {{$producto->memoria }}<br>
                            <b>Almacenamiento: </b> {{$producto->almacenamiento }}<br>
                            <b>Tarjeta Grafica: </b> {{$producto->tarjetaGrafica }}<br>
                        
                        <!--Categoria portatiles-->

                        @elseif($producto->categoria == 'portatiles')
                        
                            <b>Marca: </b> {{$producto->marca }}<br>
                            <b>Procesador: </b> {{$producto->procesador }}<br>
                            <b>Memoria: </b> {{$producto->memoria }}<br>
                            <b>Almacenamiento: </b> {{$producto->almacenamiento }}<br>
                            <b>Tarjeta Grafica: </b> {{$producto->tarjetaGrafica }}<br>
                            <b>Bateria: </b> {{$producto->bateria }}<br>
                            <b>Pulgadas: </b> {{$producto->pulgadas }}<br>

                         <!--Categoria telefonos-->

                        @elseif($producto->categoria == 'telefonos')

                            <b>Marca: </b> {{$producto->marca }}<br>
                            <b>Procesador: </b> {{$producto->procesador }}<br>
                            <b>Memoria: </b> {{$producto->memoria }}<br>
                            <b>Almacenamiento: </b> {{$producto->almacenamiento }}<br>
                            <b>Bateria: </b> {{$producto->bateria }}<br>
                            <b>Pulgadas: </b> {{$producto->pulgadas }}<br>

                         <!--Categoria perifericos-->

                        @elseif($producto->categoria == 'perifericos')
                        
                            <b>Marca: </b> {{$producto->marca }}<br>
                            <b>Conectividad: </b> {{$producto->conectividad }}<br>

                         <!--Categoria televisiones-->

                        @elseif($producto->categoria == 'televisores')
                        
                            <b>Marca: </b> {{$producto->marca }}<br>
                            <b>Pulgadas: </b> {{$producto->pulgadas }}<br>
                            <b>Conectividad: </b> {{$producto->conectividad }}<br>
                            <b>Tipo de pantalla: </b> {{$producto->tipoPantalla }}<br>

                         <!--Categoria consolas-->

                        @elseif($producto->categoria == 'consolas')
                        
                            <b>Marca: </b> {{$producto->marca }}<br>

                         <!--Categoria impresoras-->

                        @elseif($producto->categoria == 'impresoras')
                        
                            <b>Marca: </b> {{$producto->marca }}<br>
                            <b>Conectividad: </b> {{$producto->conectividad }}<br>
                            <b>Tipo de impresión: </b> {{$producto->tipoImpresion }}<br>

                         <!--Categoria electrodomesticos-->

                        @elseif($producto->categoria == 'electrodomesticos')

                            <b>Marca: </b> {{$producto->marca }}<br>
                            <b>Altura: </b> {{$producto->altura }}<br>
                            <b>Anchura: </b> {{$producto->anchura }}<br>
                            
                        @endif
                        
                        @if($producto->otros1 != null)

                            <b>Otros: </b> {{$producto->otros1 }}, {{$producto->otros2 }}, {{$producto->otros3 }}<br>
                            
                        @endif
                        <br>
                </section>

                <section id="opinionesProd">
                    <h1 class="pl-2">Reseñas<h1>
                    <hr>  
            <!--Carga todas las reseñas-->           
            @for($i = 0; $i < $nRes ; $i++)     
                    <div class="pb-5">
                        <div class="row">
                            <div class="col-2 col-sm-2 col-md-2 col-lg-1 col-xl-1 ml-2">
                                <img src="../usuario/fotoPerfil/{{$resenas[$i]['id_cliente']}}/{{$imagenUser[$i]['imagenPerfil']}}" id="imagenUsuarioResena">
                            </div>                
        
                <!--Si no ha iniciado sesion, no podra ver ningún form-->

                @guest

                <!--Si ha iniciado sesion, podrá ver todos los form-->

                @else

                    <!--Si es Admin podra Elimianr y editar todas las reseñas-->

                    @if(Auth::user()->esAdmin == 'true')
                
                            <div class="eleiminarBotResen">
                                <form action="{{ route('resenas.destroy',$resenas[$i]->id) }}" method="POST">
                                    @csrf @method('DELETE')
                                    <button type="submit" id="botonEliminarCarrito"><h3><i class="fa fa-close"></i></h3></button>
                                </form>
                            </div>
                           
                            <div class="col-9 col-sm-9 col-md-9 col-lg-10 col-xl-10">
                                <form action="{{ route('resenas.update',$resenas[$i]->id) }}" method="POST">
                                    @csrf @method('PUT')
                                    <input type="text" name="id_producto" value="{{$resenas[$i]['id_producto']}}" style="display:none;"> 
                                    <input type="text" name="id_cliente" value="{{$resenas[$i]['id_cliente']}}" style="display:none;">
                                    <input type="text" name="nombre" value="{{$resenas[$i]['nombre']}}" style="display:none;">
                                    <textarea name="descripcion" id="inputResena">{{$resenas[$i]['descripcion']}}</textarea>
                                    <input type="text" name="valoracion" value="{{$resenas[$i]['valoracion']}}" style="display:none;">

                                    <button type="sumbit" id="botonEditarResena">Editar</button>
                                </form>
                            </div>
                    @else

                    <!--Si es dueño de su reseña podrá Elimianra y editarla-->

                        @if(Auth::user()->id == $resenas[$i]['id_cliente'])
                                <div class="eleiminarBotResen">
                                    <form action="{{ route('resenas.destroy',$resenas[$i]['id']) }}" method="POST">
                                        @csrf @method('DELETE')
                                        <button type="submit" id="botonEliminarCarrito"><h3><i class="fa fa-close"></i></h3></button>
                                    </form>
                                </div>
                                <div class="col-9 col-sm-9 col-md-9 col-lg-10 col-xl-10">
                                <form action="{{ route('resenas.update',$resenas[$i]['id']) }}" method="POST">
                                    @csrf @method('PUT')
                                    <input type="text" name="id_producto" value="{{$resenas[$i]['id_producto']}}" style="display:none;"> 
                                    <input type="text" name="id_cliente" value="{{$resenas[$i]['id_cliente']}}" style="display:none;">
                                    <input type="text" name="nombre" value="{{$resenas[$i]['nombre']}}" style="display:none;">
                                    <textarea name="descripcion" id="inputResena">{{$resenas[$i]['descripcion']}}</textarea>
                                    <input type="text" name="valoracion" value="{{$resenas[$i]['valoracion']}}" style="display:none;">

                                    <button type="sumbit" id="botonEditarResena">Editar</button>
                                </form>
                            </div>
                        @else
                            <div class="col-9 col-sm-9 col-md-9 col-lg-10 col-xl-10">
                                <textarea id="inputResena" disabled>{{$resenas[$i]['descripcion']}}</textarea>
                            </div>
                        @endif

                    @endif
                @endguest

                    <!--Cliente no logeado-->

                        @guest
                        <div class="col-9 col-sm-9 col-md-9 col-lg-10 col-xl-10">
                                <textarea id="inputResena" disabled>{{$resenas[$i]['descripcion']}}</textarea>
                            </div>
                        @else
                        @endguest
                            
                        </div>
                        <p id="nombreResena">{{$resenas[$i]['nombre']}}<p>
                        <b class="rating2 ml-4 pr-1">
                            @for ($k = 0; $k < $resenas[$i]['valoracion'] ; $k++)   
                                    <i class="fa fa-star puntuacionReseñas"></i>
                            @endfor
                        </b>                    
                    </div>
                           
                    <hr>

            @endfor

            @guest
            @else

                <!--Si El usuario esta logeado puede escribir una reseña -->
                     
                @if($id_clienteResena !=  Auth::user()->id )
                    <form action="{{ route('resenas.store') }}" method="POST">
                        @csrf
                        
                        <div class="row">
                           
                            <div class="col-2 col-sm-2 col-md-2 col-lg-1 col-xl-1 ml-2">
                                <img src="../usuario/fotoPerfil/{{Auth::user()->id}}/{{Auth::user()->imagenPerfil}}" id="imagenUsuarioResena">
                            </div>
                       
                            <div class="col-9 col-sm-9 col-md-9 col-lg-10 col-xl-10">
                                <textarea id="inputResena" name="descripcion" placeholder="Escribe tu comentario"></textarea>
                            </div>
                        </div>
                             
                        <b class="rating ml-4">
                            <input type="radio" name="rating" value="5" id="5"><label for="5">☆</label>
                            <input type="radio" name="rating" value="2" id="4"><label for="4">☆</label>
                            <input type="radio" name="rating" value="3" id="3"><label for="3">☆</label>
                            <input type="radio" name="rating" value="4" id="2"><label for="2">☆</label>
                            <input type="radio" name="rating" value="1" id="1"><label for="1">☆</label>
                        </b>  
                    @guest
                            <button disabled style="font-size:18px; float:right;" class="mr-5">Publicar</button>
                    @else
                            <input type="text" name="id_producto" value="{{$producto->id}}" style="display:none;"> 
                            <input type="text" name="id_cliente" value="{{Auth::user()->id}}" style="display:none;">
                            <input type="text" name="nombre" value="{{Auth::user()->name}}" style="display:none;">
                            <input type="text" name="valoracion" id="valoracionRes" style="display:none;">

                        <button type="sumbit" style="font-size:18px; float:right;" class="mr-5">Publicar</button>
                    @endguest
                    </form>
                @else
                    <h5 class="text-center">Solo puedes publicar una reseña por producto</h5>
                @endif
            @endguest
                </section>
            </article>
            
            <aside id="imagenesProd" class="col-lg-2 col-xl-2 d-none d-sm-none d-md-none d-lg-block d-xl-block">
            @foreach ($destacados as $destacado)
                <a href="{{ route('productos.show',$destacado->id) }}"><div class="cajasImagenesCat">
                    <img src="../producto/{{ $destacado->categoria }}/{{ $destacado->id }}/{{ $destacado->imagen1 }}">
                    <p class="text-center textoProdCat" >{{ $destacado->nombre }}</p>
                    <p class="text-center textoProdCat" >{{ $destacado->precio }}€</p>
                </div></a><hr>
            @endforeach
            </aside>
        </div>
    </div>

    </div>

@include('productos.Includes.footers.footer')