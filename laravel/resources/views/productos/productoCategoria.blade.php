@include('productos.Includes.headers.header-links')
<!--Logo Empresa-->         
        <div>
            <a href="/productos"><img src="../LogoEmrpesa.png"  
            style="cursor: pointer; width: 130px; height: 40px; margin-top: 12px; margin-right: 15px;"></a>
        </div>
@include('productos.Includes.headers.header')




 <!--Mapa de Navegación-->
        <nav>
            <div class="row">
                <div id="mapaNavegacion" class="col-12 col-sm-7 col-md-5 col-lg-5 col-xl-5"><a href="/productos">Inicio</a> > {{ $categoriasName }}</div>
                <div id="textoPagina" class="d-none d-sm-block d-md-block d-lg-block col-sm-5 col-md-7 col-lg-7 col-xl-7"></div>
            </div>
        </nav>

<article class="col-sm-12 col-md-12 col-lg-12 col-xl-12">

    <!--Desplegable de filtros -->



        <div></div>
        <div class="row">
            <!-- FIltro Productos    -->
            <aside id="filtroProducto" class="d-none d-sm-none d-md-none d-lg-block d-xl-block">
                <div class="filtroClaseTitulo row">
                    <p class="filtroClaseNombre">Producto 1</p>
                    <p class="desplegableFiltroMenos fa fa-minus" data-value="2"></p>
                    <p id="desplegableFiltroMas2" class="desplegableFiltroMas fa fa-plus" data-value="2"></p>
                    <div class="desplegableFiltroBarra"></div>
                </div>

                <div id="cajas2">
                    <input type="checkbox" class="filtroClasesCaja"> Algo 1 <br>
                    <input type="checkbox" class="filtroClasesCaja"> Algo 2
                </div>
                
                <div class="filtroClaseTitulo row">
                    <p class="filtroClaseNombre"> Producto 2</p>
                    <p class="desplegableFiltroMenos fa fa-minus" data-value="3"></p>
                    <p id="desplegableFiltroMas3" class="desplegableFiltroMas fa fa-plus" data-value="3"></p>
                    <div class="desplegableFiltroBarra"></div>
                </div>

                <div id="cajas3">
                    <input type="checkbox" class="filtroClasesCaja"> Algo 1 <br>
                    <input type="checkbox" class="filtroClasesCaja"> Algo 2
                </div>
                
                <div class="filtroClaseTitulo row">
                    <p class="filtroClaseNombre">Producto 3</p>
                    <p class="desplegableFiltroMenos fa fa-minus" data-value="4"></p>
                    <p id="desplegableFiltroMas4" class="desplegableFiltroMas fa fa-plus" data-value="4"></p>
                    <div class="desplegableFiltroBarra"></div>
                </div>
                <div id="cajas4">
                    <input type="checkbox" class="filtroClasesCaja"> Algo 1 <br>
                    <input type="checkbox" class="filtroClasesCaja"> Algo 2 <br>
                    <input type="checkbox" class="filtroClasesCaja"> Algo 3
                </div>
                <div class="filtroClaseTitulo row">
                    <p class="filtroClaseNombre">Producto 4</p>
                    <p class="desplegableFiltroMenos fa fa-minus" data-value="5"></p>
                    <p id="desplegableFiltroMas5" class="desplegableFiltroMas fa fa-plus" data-value="5"></p>
                    <div class="desplegableFiltroBarra"></div>
                </div>
                <div id="cajas5">
                    <input type="checkbox" class="filtroClasesCaja"> Algo 1 <br>
                    <input type="checkbox" class="filtroClasesCaja"> Algo 2 <br>
                    <input type="checkbox" class="filtroClasesCaja"> Algo 3
                </div>
            </aside>

            <section id="SectProdCat" class="col-sm-11 col-md-12 col-lg-8 col-xl-9">
                
            <div class="row"> 

                    <h1 style="text-transform:uppercase;" class="col-8 col-sm-8 col-md-8 col-lg-8 col-xl-9">{{ $categoriasName }}</h1>  
        @guest

        @else

            @if(Auth::user()->esAdmin == 'true')
                    <a class="btn btn-success col-3 col-sm-4 col-md-3 col-lg-3 col-xl-2" style="height:40px" href="{{ route('productos.create') }}"> Crear producto</a>  
            @endif

        @endguest
                </div>
            <div class="row" style="margin-top:50px;">
                <p id="numProd"><b>{{ $numCategoria }}</b> Productos</p>
                <div class="col-4 col-sm-5 col-md-7 col-lg-6 col-xl-8"></div>
                <select id="SelectFiltro" onchange="FiltrarX()">
                    <option value="Relevancia">Relevancia</option>
                    <option value="Asc">Precio Ascendente</option>
                    <option value="Des">Precio Descendiente</option>
                    <option value="Mvendidos">Más vendidos</option>
                </select>
            </div>
            <hr>
            @if( $numCategoria == 0 )
                <h1 class="text-center">No hay prodcutos en esta categoria</h1>
            @endif
            <!--Productos Relevancia-->
                <div id="ordRelevancia"> 
                    @foreach ($categorias as $categoria)
                        <div class="productosShow">
                            <a href="{{ route('productos.show',$categoria->id) }}">
                            <img src="../producto/{{ $categoria->categoria }}/{{ $categoria->id }}/{{ $categoria->imagen1 }}" class="imagenesRes" >
                            
                            <p style="text-align:center;" >{{ $categoria->nombre }} </p>
                            <div class="row">
                                <div class="col-sm-4 col-md-4 col-lg-4 col-xl-4"></div>
                                <h5 class="col-sm-4 col-md-4 col-lg-4 col-xl-4" style="text-align:center;" ><b>{{ $categoria->precio }}</b>€</h5>
                                <div class="col-sm-4 col-md-4 col-lg-4 col-xl-4"></div>
                            </div>
                            </a>
                        </div>
                    @endforeach  
                </div>


            <!--Productos Orden Ascendente-->    
                <div id="ordAsc"> 
                   @foreach ($productoAsc as $prodAsc)
                        <div style="width:33%; float:left;">
                            <a href="{{ route('productos.show',$prodAsc->id) }}">
                                <img src="../producto/{{ $prodAsc->categoria }}/{{ $prodAsc->id }}/{{ $prodAsc->imagen1 }}" class="imagenesRes" >
                            </a>
                            <p style="text-align:center;">{{ $prodAsc->nombre }} </p>
                            <div class="row">
                                <div class="col-sm-4 col-md-4 col-lg-4 col-xl-4"></div>
                                <h5 class="col-sm-4 col-md-4 col-lg-4 col-xl-4" style="text-align:center;"><b>{{ $prodAsc->precio }}</b>€</h5>
                                <div class="col-sm-4 col-md-4 col-lg-4 col-xl-4"></div>
                            </div>
                        </div>
                    @endforeach 
                </div>


            <!--Productos Orden Descendente--> 
                <div id="ordDesc"> 
                    @foreach ($productoDesc as $prodDesc)
                        <div style="width:33%; float:left;">
                            <a href="{{ route('productos.show',$prodDesc->id) }}">
                                <img src="../producto/{{ $prodDesc->categoria }}/{{ $prodDesc->id }}/{{ $prodDesc->imagen1 }}" class="imagenesRes" >
                            </a>
                            <p style="text-align:center;">{{ $prodDesc->nombre }} </p>
                            <div class="row" >
                                <div class="col-sm-4 col-md-4 col-lg-4 col-xl-4"></div>
                                <h5 class="col-sm-4 col-md-4 col-lg-4 col-xl-4" style="text-align:center;"><b>{{ $prodDesc->precio }}</b>€</h5>
                                <div class="col-sm-4 col-md-4 col-lg-4 col-xl-4"></div>
                            </div>
                        </div>
                    @endforeach
                </div> 


                <!--Productos Orden Descendente--> 
                <div id="ordVendidos"> 
                    @foreach ($productoVendido as $prodVend)
                        <div style="width:33%; float:left;">
                            <a href="{{ route('productos.show',$prodVend->id) }}">
                                <img src="../producto/{{ $prodVend->categoria }}/{{ $prodVend->id }}/{{ $prodVend->imagen1 }}" class="imagenesRes" >
                            </a>
                            <p style="text-align:center;">{{ $prodVend->nombre }} </p>
                            <div class="row">
                                <div class="col-sm-4 col-md-4 col-lg-4 col-xl-4"></div>
                                <h5 class="col-sm-4 col-md-4 col-lg-4 col-xl-4" style="text-align:center;"><b>{{ $prodVend->precio }}</b>€</h5>
                                <div class="col-sm-4 col-md-4 col-lg-4 col-xl-4"></div>
                            </div>
                        </div>
                    @endforeach
                </div>

            </section>
        </div>
    </article>  
        <div id="botonFiltroDesaparecen">
            <ion-icon name="chevron-up-outline" class="chevron ml-1 mt-1"></ion-icon>
        </div>
    <script>
        $(".desplegableFiltroMenos").click(function (e) {
            $("#cajas" + e.currentTarget.dataset.value).toggle("slow");
            $('#desplegableFiltroMas' + e.currentTarget.dataset.value).css("display", "block");
           
        });
        $(".desplegableFiltroMas").click(function (e) {
            $("#cajas" + e.currentTarget.dataset.value).toggle("slow");
            $('#desplegableFiltroMas' + e.currentTarget.dataset.value).css("display", "none");
        });
    
        
        function FiltrarX() {
            var SelectFiltro = document.getElementById("SelectFiltro").value;

          if(SelectFiltro == "Relevancia"){
            document.getElementById("ordRelevancia").style.display = "block";
            document.getElementById("ordDesc").style.display = "none";
            document.getElementById("ordAsc").style.display = "none";
            document.getElementById("ordVendidos").style.display = "none";
          }

          if(SelectFiltro == "Asc"){
            document.getElementById("ordRelevancia").style.display = "none";
            document.getElementById("ordDesc").style.display = "none";
            document.getElementById("ordAsc").style.display = "block";
            document.getElementById("ordVendidos").style.display = "none";
          }

          if(SelectFiltro == "Des"){
            document.getElementById("ordRelevancia").style.display = "none";
            document.getElementById("ordDesc").style.display = "block";
            document.getElementById("ordAsc").style.display = "none";
            document.getElementById("ordVendidos").style.display = "none";
          }

          if(SelectFiltro == "Mvendidos"){
            document.getElementById("ordRelevancia").style.display = "none";
            document.getElementById("ordDesc").style.display = "none";
            document.getElementById("ordAsc").style.display = "none";
            document.getElementById("ordVendidos").style.display = "block";
          }
        }
    </script>
    
@include('productos.Includes.footers.footer')

