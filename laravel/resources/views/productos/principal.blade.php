@include('productos.Includes.headers.header-links')

<!--Logo Empresa-->         
        <div>
            <a href="/productos"><img src="LogoEmrpesa.png"  
            style="cursor: pointer; width: 130px; height: 40px; margin-top: 12px; margin-right: 15px;"></a>
        </div>
@include('productos.Includes.headers.header')
    
<!--Mapa de Navegación-->
        <nav>
            <div class="row">
                <div id="mapaNavegacion" class="col-12 col-sm-7 col-md-5 col-lg-5 col-xl-5">Inicio </div>
                <div id="textoPagina" class="d-none d-sm-block d-md-block d-lg-block col-sm-5 col-md-7 col-lg-7 col-xl-7"></div>
            </div>
        </nav>
    <article style="position: relative; top:110px;"> 

        <div id="carruselImg" class="carousel slide" data-ride="carousel">

            <!-- Indicadores -->
            <ul class="carousel-indicators">
                <li data-target="#carruselImg" data-slide-to="0" class="active"></li>
                <li data-target="#carruselImg" data-slide-to="1"></li>
                <li data-target="#carruselImg" data-slide-to="2"></li>
            </ul>

            <!-- Imagenes Carrusel -->
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img style=" display: block;margin-left: auto; margin-right: auto;" src="https://ace.electronicsforu.com/wp-content/uploads/2018/02/consumer-electronic-products.jpg" width="900" height="300">
                </div>
                <div class="carousel-item">
                    <img style=" display: block;margin-left: auto; margin-right: auto;" src="https://ace.electronicsforu.com/wp-content/uploads/2018/02/consumer-electronic-products.jpg" width="900" height="300">
                </div>
                <div class="carousel-item">
                    <img style=" display: block;margin-left: auto; margin-right: auto;" src="https://ace.electronicsforu.com/wp-content/uploads/2018/02/consumer-electronic-products.jpg" width="900" height="300">
                </div>
            </div>

            <!-- Izq y derecha controlles -->
            <a class="carousel-control-prev" href="#carruselImg" data-slide="prev"></a>
            <a class="carousel-control-next" href="#carruselImg" data-slide="next"></a>
            
        </div>

        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12"> 

            <div class="row pb-3">
                <b class="etiquetas">Destacados</b>
                <div class="lineaEtiqueta col-5 col-sm-7 col-md-8 col-lg-9 col-xl-9 mt-3"></div>
                <a href="/destacados" class="enlaces">Ver todas</a>
            </div>
                
            <section class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                @foreach ($destacados as $dest)
                    <div id="productosShowPrinc">
                        <a href="{{ route('productos.show',$dest->id) }}">
                            <div style="width: 100%; height: auto;">
                                <img src="producto/{{ $dest->categoria }}/{{ $dest->id }}/{{ $dest->imagen1 }}" class="imagenesRes" >
                            </div>
                        <p style="margin-left:20%; display: flex;justify-content: center; white-space: nowrap; overflow: hidden; width:200px;" >{{ $dest->nombre }} </p>
                        <div class="row">
                            <div class="col-sm-4 col-md-4 col-lg-4 col-xl-4"></div>
                                <h5 class="col-sm-4 col-md-4 col-lg-4 col-xl-4" style="text-align:center;" ><b>{{ $dest->precio }}</b>€</h5>
                            <div class="col-sm-4 col-md-4 col-lg-4 col-xl-4"></div>
                         </div>
                        </a>
                    </div>
                @endforeach  
            </section>

            <div class="row pt-3 pb-3">
                <b class="etiquetas">Recomendaciones</b>
                <div class="lineaEtiqueta col-4 col-sm-6 col-md-7 col-lg-8 col-xl-9 mt-3"></div>
                <a  href="/recomendados" class="enlaces">Ver todas</a>
            </div>

            <section class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                @foreach ($recomendaciones as $reco)
                    <div id="productosShowPrinc">
                        <a href="{{ route('productos.show',$reco->id) }}">
                            <div style="width: 100%; height: auto;">
                                <img src="producto/{{ $reco->categoria }}/{{ $reco->id }}/{{ $reco->imagen1 }}" class="imagenesRes" >
                            </div>
                        <p style="margin-left:20%; display: flex;justify-content: center;white-space: nowrap; overflow: hidden;width:200px;" >{{ $reco->nombre }} </p>
                        <div class="row">
                            <div class="col-sm-4 col-md-4 col-lg-4 col-xl-4"></div>
                                <h5 class="col-sm-4 col-md-4 col-lg-4 col-xl-4" style="text-align:center;" ><b>{{ $reco->precio }}</b>€</h5>
                            <div class="col-sm-4 col-md-4 col-lg-4 col-xl-4"></div>
                         </div>
                        </a>
                    </div>
                @endforeach  
            </section>
        </div>
    </article>
    

    
    <div id="footerFix"></div>

    @include('productos.Includes.footers.footer')
