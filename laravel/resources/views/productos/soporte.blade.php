@include('productos.Includes.headers.header-links')

    <!--Logo Empresa-->         
        <div>
            <a href="/productos"><img src="LogoEmrpesa.png" id="imagenEmpresa" 
            style="cursor: pointer; width: 130px; height: 40px; margin-top: 12px; margin-right: 15px;"></a>
        </div>
@include('productos.Includes.headers.header')
    
    <!--Mapa de Navegación-->
        <nav>
            <div class="row">
                <div id="mapaNavegacion" class="col-12 col-sm-7 col-md-5 col-lg-5 col-xl-5"><a href="/productos">Inicio</a> > Soporte</div>
                <div id="textoPagina" class="d-none d-sm-block d-md-block d-lg-block col-sm-5 col-md-7 col-lg-7 col-xl-7"></div>
            </div>
        </nav>
<div class="container-fluid">
    
    <div class="row" style="padding-top:88px">
        <aside class="col-xl-4  d-none d-sm-none d-md-none d-lg-none d-xl-block">
            <img src="https://www.eduweb.es/wp-content/uploads/2016/08/soporte-tecnico-web-barcelona-eduweb.png" class="imagenesRes">
        </aside>
        <article class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-7 pt-3">
            <h1 class=text-center>Centro de soporte</h1>
            <div class="row pt-4">
                <div class="col-12 col-sm-12 col-md-7 col-lg-8  col-xl-6">
                    <p>Ponte en contacto con ElecroMes si necesitas asistencia. Los clientes que actualmente tienen un contrato de mantenimiento tienen acceso al soporte técnico y a nuestros recursos en línea como se describe en las Políticas y Procedimientos de Soporte Técnico en el sitio web. Invitamos a todos los clientes a acceder al sitio de asistencia de ElectroMes como primer paso para resolver cualquier inconveniente. Los clientes también pueden encontrar información útil en la ‎‏‎Comunidad de ElectroMes. Si necesitas asistencia adicional, la persona de contacto de soporte designada para el cliente puedes comunicarte con asistencia técnica para que registre un nuevo caso de soporte utilizando el sitio de asistencia, la información de correo electrónico que figura a continuación para saber dónde se gestionaron tus licencias.</p>
                </div>
                <form role="form" method="post" action="{{ route('sendmail') }}" enctype="multipart/form-data">
	                @csrf
                    <div class="col-5 col-sm-5 col-md-5 col-lg-5 col-lg-5 col-xl-5">
                        <textarea placeholder="Descripción del incoviniente" name="descripcion" style="border: 1px solid black;  resize: none; height:290px; width: 280px;"></textarea>
                        <div class="row">
                            <input type="text" name="correo" placeholder="Correo" class="ml-3">
                            <button class="ml-3 mt-2" style="width:">Envíar</button>
                        </div>
                    </div>
                </form>
            </div>  
        </article>
    </div> 
</div>

@include('productos.Includes.footers.footer')