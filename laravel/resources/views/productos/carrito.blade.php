    @include('productos.Includes.headers.header-links')

    <!--Logo Empresa-->         
    <div class="ml-4">
        <a href="/productos"><img src="../LogoEmrpesa.png" id="imagenEmpresa" style="cursor: pointer; width: 130px; height: 40px; margin-top: 12px; margin-right: 15px;"></a>
    </div>
    <!--Mapa de Navegación-->
            <nav>
                <div class="row">
                    <div id="mapaNavegacion" class="col-12 col-sm-7 col-md-5 col-lg-5 col-xl-5"><a href="/productos">Inicio</a> > Carrito</div>
                    <div id="textoPagina" class="d-none d-sm-block d-md-block d-lg-block col-sm-5 col-md-7 col-lg-7 col-xl-7"></div>
                </div>
            </nav>
    <div class="container-fluid">
    <div id="pago">
        <article>
            <div class="pt-3">
                <div class="row pl-5">
                    <h3 id="direccionEnvio" style="cursor:pointer">Dirección de Envío</h3> 
                    <a href="#" id="cambiarEnvio"><h6 class="pt-2 pl-5">Cambiar Dirección de envío</h6></a>
                </div>
                <!-- Datos de envío usuario -->
                <div class="row" >
                    <div class="col-1 col-sm-1 col-md-1 col-lg-1 col-xl-1"></div>
                    <div class="col-10 col-sm-10 col-md-10 col-lg-10 col-xl-10 pt-2 direccionEnvio cajaPago">
                        @if(Auth::user()->calle == "")
                            <p>No hay datos de Envío</p>
                        @else
                            <h4>{{Auth::user()->name}} {{Auth::user()->primerapellido}} {{Auth::user()->segundoapellido}}</h4>
                            <h6>{{Auth::user()->calle}}, {{Auth::user()->numeroCalle}} {{Auth::user()->letraCalle}}</h6> 
                            <h6>{{Auth::user()->codigoPostal}} {{Auth::user()->ciudad}}</h6>
                            <h6>Tel {{Auth::user()->telefono}}</h6>
                        @endif
                    </div>     
                </div>
               
                <div class="row">
                    <div class="col-1 col-sm-1 col-md-1 col-lg-1 col-xl-1"></div>
                    <div class="col-10 col-sm-10 col-md-10 col-lg-10 col-xl-10 pt-2 direccionEnvio1 cajaPago" style="display: none;">
                    <form action="{{ route('detalles-usuario.update', 'datos' ) }}" method="POST">
                        @csrf @method('PUT')
                        <input type="text" name="calle" placeholder="Calle" class="col-8">
                        <input type="text" name="numeroCalle" placeholder="Número"><br>
                        <input type="text" name="letraCalle" placeholder="Letra" class="mt-3">
                        <input type="text" name="codigoPostal" placeholder="Codigo Postal"  class="mt-3" >
                        <input type="text" name="ciudad" placeholder="Ciudad" class="mt-3">
                        <button type="sumbit" class="mt-2">Guardar</button>
                        </form>   
                    </div> 
                </div>
              
                <h3 id="direccionEnvio" class="ml-5" style="cursor:pointer">Metodos de pago</h3> 

                <div class="row">
                    <div class="col-1 col-sm-1 col-md-1 col-lg-1 col-xl-1"></div>
                    <div id="metodoPago" class="col-10 col-sm-10 col-md-10 col-lg-10 col-xl-10 pt-2 cajaPago">
                        <div class="d-none d-sm-none d-md-block d-lg-block d-xl-block">
                        <div class="ml-5"></div>
                            <input type="checkbox" class="ml-5"> Tarjeta<input type="checkbox" class="ml-5"> Financiazión 
                            <input type="checkbox" class="ml-5"> Transferencía<input type="checkbox" disabled class="ml-5"> Contrarembolso
                        </div>
                        <div class="d-block d-sm-block d-md-none d-lg-none d-xl-none">
                            <input type="checkbox"> Tarjeta<input type="checkbox"> Financiazión<br>
                            <input type="checkbox"> Transferencía <input type="checkbox" disabled> Contrarembolso
                        </div>
                    </div>
                </div>

                <h3 id="direccionEnvio" class="ml-5" style="cursor:pointer">Detalles de pedido</h3>

                <div class="row">
                    <div class="col-1 col-sm-1 col-md-1 col-lg-1 col-xl-1"></div>
                    <div id="metodoPago" class="col-10 col-sm-10 col-md-10 col-lg-10 col-xl-10 pt-2 cajaPago1">
                @for ($i = 0; $i < $numCarr ; $i++)
                        <div class="row mb-2">
                            <div class="col-4 col-sm-4 col-md-4 col-lg-3 col-xl-3">
                                <a href="{{ route('productos.show', $datos[$i]->id_producto) }}"> 
                                    <img src="../producto/{{ $datosProducto[$i]['categoria']}}/{{ $datosProducto[$i]['id']}}/{{ $datosProducto[$i]['imagen1']}}" class="imagenesRes" ></a>
                            </div>
                            <div class="col-7 col-sm-7 col-md-7 col-lg-8 col-xl-9">
                                <a href="{{ route('productos.show', $datos[$i]->id_producto) }}"> 
                                <h5 class="mt-3" style="overflow:hidden; height:52px">{{ $datos[$i]->nombreProducto }}</h5>
                                </a>
                                <p>Unidades: {{$datos[$i]->cantidad}}</p>

                                <div class="row">
                                    <h5 class="col-4 col-sm-5 col-md-5 col-lg-7 col-xl-8"><b>{{ $datos[$i]->precioProducto }}</b>€</h5>
                                </div>
                            </div> 
                        </div>
                @endfor 
                    </div>
                </div>
            <form action="{{ route('pago.store') }}" method="POST">
                    @csrf
                        <input type="text" name="id_cliente" value="{{Auth::user()->id}}" style="display:none;"> 
                    @if($datos == null)
                        <input type="text" name="id_producto" value="{{$datos[0]->id_producto}}" style="display:none;">
                    @else
                        <input type="text" name="id_producto" value="" style="display:none;">
                    @endif
                        <input type="text" name="fechaEnvio" value="<?php echo date("d-m-Y")?>" style="display:none;">
                        <input type="text" name="fechaLlegada" value="<?php $dia= date("d"); $dia = $dia + 2; echo date($dia."-m-Y")?>" style="display:none;">
                        <input type="text" name="precioTotal" value="{{$precioTotal}}" style="display:none;"> 
                        <input type="text" name="calle" value="{{Auth::user()->calle}}" style="display:none;">
                        <input type="text" name="numero_direccion" value="{{Auth::user()->numeroCalle}}" style="display:none;">
                        <input type="text" name="letra_direccion" value="{{Auth::user()->letraCalle}}" style="display:none;">
                        <input type="text" name="ciudad" value="{{Auth::user()->ciudad}}" style="display:none;">
                        <input type="text" name="codigo_postal" value="{{Auth::user()->codigoPostal}}" style="display:none;">
                        <input type="text" name="provincia" value="{{Auth::user()->provincia}}" style="display:none;">
                    @if($datos == null)
                        <input type="text" name="cantidad" value="{{$datos[0]->cantidad}}" style="display:none;">
                    @else
                        <input type="text" name="cantidad" value="" style="display:none;">
                    @endif              
                    <button type="sumbit" class="float-right mr-5 mt-3">Pagar</button>
            </form>
                <button class="float-right mr-5 mt-3 atrasPago">Atras</button>
            </div>
        </article>
   
    </div> 
    <div style="margin-top:50px"></div>
            <h3>{{$numCarr}} Articulo en el carrito</h3>
            <div class="row">
                <article class="col-sm-12 col-md-8 col-lg-8 col-xl-8">
            
                <!--<div class="d-none d-xl-block">xl</div>
                <div class="d-none d-xl-none d-lg-block">lg</div>
                <div class="d-none d-lg-none d-xl-none d-md-block">md</div>
                <div class="d-block d-md-block d-lg-block d-xl-block d-sm-block">sm</div>-->
                 
                    
                
                    <section class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" id="cajaProdCarrito">
                    @for ($i = 0; $i < $numCarr ; $i++) 
                        <div class="row mb-2">
                            <div class="col-5 col-sm-5 col-md-5 col-lg-4 col-xl-3">
                                <a href="{{ route('productos.show', $datos[$i]->id_producto) }}"> 
                                    <img src="../producto/{{ $datosProducto[$i]['categoria']}}/{{ $datosProducto[$i]['id']}}/{{ $datosProducto[$i]['imagen1']}}" class="imagenesRes" ></a>
                            </div>
                            <div class="col-7 col-sm-7 col-md-7 col-lg-8 col-xl-9">
                                <a href="{{ route('productos.show', $datos[$i]->id_producto) }}"> 
                                <h5 class="mt-3" style="overflow:hidden; height:52px">{{ $datos[$i]->nombreProducto }}</h5>
                                </a>
                                <p>Unidades: {{$datos[$i]->cantidad}}</p>

                                <div class="row">
                                    <h5 class="col-4 col-sm-5 col-md-5 col-lg-7 col-xl-8"><b>{{ $datos[$i]->precioProducto }}</b>€</h5>
                                    <form action="{{ route('carrito.update',$datos[$i]->id) }}" method="POST">
                                        @csrf

                                        @method('PUT')
                                        <input type="text" name="id_producto" value="{{$datos[$i]->id_producto}}" style="display:none;">
                                        <input type="text" name="nombreProducto" value="{{$datos[$i]->nombreProducto}}" style="display:none;">
                                        <input type="text" name="id_cliente" value="{{$datos[$i]->id_cliente}}" style="display:none;">
                                        <input type="text" name="precioProducto" value="{{$datos[$i]->precioProducto}}" style="display:none;">
                                        <div style="position:relative; bottom:6px">
                                            <button class="btnCevron"><ion-icon name="chevron-back-outline"  class="chevron"></ion-icon></button>
                                            <input type="number" name="cantidad" min="0" max="99" id="inputCantidadCarrito" value="{{$datos[$i]->cantidad}}">
                                            <button class="btnCevron"><ion-icon name="chevron-forward-outline"  class="chevron"></ion-icon></button>
                                        </div>
                                        
                                    </form>         
                                        
                                    <div class="col-1 col-sm-1 col-md-1 col-lg-1">
                                        <form action="{{ route('carrito.destroy',$datos[$i]->id_producto) }}" method="POST">
                                            @csrf @method('DELETE')
                                            <button type="submit" id="botonEliminarCarrito"><i class="fa fa-close "></i></button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        @endfor  
                    </section>
                </article>
                <br>
            
                <aside class="col-sm-12 col-md-4 col-lg-4 col-xl-4 d-none d-sm-none d-md-block d-lg-block d-xl-block" >
                    <div style="height: 370px; border: 2px solid black;">
                        <h4 class="text-center mt-2">Detalles de pedido</h4>
                        <div style="height:190px; overflow:auto;">
                            @foreach ($datos as $prod)
                                <div class="row" style="height:27px; width:100%;">
                                    <p class="ml-4" style="width:8%;">x{{$prod->cantidad}}</p>
                                    <p id="prodEnRelaizarPed"> <a href="{{ route('productos.show', $prod->id_producto) }}">{{$prod->nombreProducto}}</a></p>
                                </div>
                            @endforeach
                        </div>  
                        <hr class="mt-2">
                        
                        <div class="row justify-content-center mt-2 mb-3">
                            <h5 class="mt-2">Total </h5> 
                            <h5 class="mt-2">:<b> {{$precioTotal}} €</b></h5>
                        </div>
  
                        

                        <div class="row justify-content-center mb-3">
                            
                                
                   
                             
                                <button class="btn btn-dark realizarPago" type="sumbit">Realizar pago</button>
                            
                        <!-- adasda -->
                        </div>  
                    </div>
                </aside>
                <aside class="col-sm-12 col-md-12 col-lg-12 col-xl-12 d-block d-sm-block d-md-none d-lg-none d-xl-none">
                    <div id="detallesDespleableCarro" class="pt-2">
                        <div style="height:250px;">
                            @foreach ($datos as $prod)
                                <div class="row" style="height:27px;">
                                    <p class="ml-4">x{{$prod->cantidad}}</p>
                                    <p id="prodEnRelaizarPed">-  <a href="{{ route('productos.show', $prod->id_producto) }}">{{$prod->nombreProducto}}</a> </p>
                                </div>
                            @endforeach
                        </div>
                        <div class="row float-right mr-3">
                            <h5 class="mt-2">Total </h5> 
                            <h5 class="mt-2">:<b> {{$precioTotal}} €</b></h5>
                        </div>
                    </div>
                    <div class="row mt-3">

                        <h4 class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">Total: <b>{{$precioTotal}}€</b></h4>
                        
                        <button class="btn btn-dark col-3 col-sm-3 col-md-3 col-lg-3 col-xl-3 realizarPago">Realizar pago</button>
                        <button class="btn btn-dark col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2 ml-3" id="verMasCarrito" ><i class="fa fa-angle-up"></i></button>
                        <button class="btn btn-dark col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2 ml-3" id="verMenosCarrito" style="display:none;"><i class="fa fa-angle-down"></i></button>

                    </div>  
                </aside>
            </div>
                <div class="col-sm-6 col-md-6 col-lg-12 mt-1">
                    <h3>Formas de pago</h3>
                    <img src="https://sistemahost.com/images/metodos-pago.png" class="col-5 col-sm-12 col-md-8 col-lg-4 col-xl-4" style="width: 100%; height: auto;">
                </div>     
                <p style="font-size: 13px;">Electro Mes CIF D63289452. Calle del Mazo 10. Polígono Industrial San José de Valderas, 28923 , Alcorcón, Madrid , ESPAÑA.</p>

    </div>

    

    <script>
        $(document).ready(function(){
            $("#verMasCarrito").click(function() { 
                $("#detallesDespleableCarro").show("slow");
                $("#verMasCarrito").hide();
                $("#verMenosCarrito").show();       
            });
            $("#verMenosCarrito").click(function() { 
                $("#detallesDespleableCarro").hide("slow");
                $("#verMasCarrito").show();
                $("#verMenosCarrito").hide();       
            });
        });

        var cantidad = [];
            $(document).ready(function(){
                cantidad[0] = document.getElementById("inputCantidadCarrito").value;
                
                if(cantidad[0] >= 0){
                    $(".chevron:eq(1)").click(function(){
                        cantidad[0]++;
                        $("#inputCantidadCarrito").val(cantidad[0]);
                    });
                }
                if(cantidad[0] >= 0){
                    $(".chevron:eq(0)").click(function(){
                        cantidad[0]--;
                        $("#inputCantidadCarrito").val(cantidad[0]);
                    });
                }

                $(".realizarPago").click(function(){
                        $("#pago").show();
                });
                $(".atrasPago").click(function(){
                        $("#pago").hide();
                });
                $("#cambiarEnvio").click(function(){
                        $(".direccionEnvio").hide();
                        $(".direccionEnvio1").show();
                });
                $("#direccionEnvio").click(function(){
                        $(".direccionEnvio").show();
                        $(".direccionEnvio1").hide();
                });
            });
    </script>
