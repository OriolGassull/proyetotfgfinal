@include('productos.Includes.headers.header-links')

<!--Logo Empresa-->         
        <div>
            <a href="/productos"><img src="LogoEmrpesa.png" id="imagenEmpresa" 
            style="cursor: pointer; width: 130px; height: 40px; margin-top: 12px; margin-right: 15px;"></a>
        </div>
@include('productos.Includes.headers.header')
    
<!--Mapa de Navegación-->
        <nav>
            <div class="row">
                <div id="mapaNavegacion" class="col-12 col-sm-7 col-md-5 col-lg-5 col-xl-5"><a href="/productos">Inicio</a> > Ver Todas</div>
                <div id="textoPagina" class="d-none d-sm-block d-md-block d-lg-block col-sm-5 col-md-7 col-lg-7 col-xl-7"></div>
            </div>
        </nav>
        <article style="position: relative; top:110px;">     
            <section class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                @foreach ($verTodas as $dest)
                    <div id="productosShowPrinc" style="border: 1px solid black;">
                        <a href="{{ route('productos.show',$dest->id) }}">
                            <div style="width: 100%; height: auto;">
                                <img src="producto/{{ $dest->categoria }}/{{ $dest->id }}/{{ $dest->imagen1 }}" class="imagenesRes" >
                            </div>
                        <p style=" margin-left:20%; display: flex;justify-content: center; white-space: nowrap; overflow: hidden; width:200px;" >{{ $dest->nombre }} </p>
                        <div class="row">
                            <div class="col-sm-4 col-md-4 col-lg-4 col-xl-4"></div>
                                <h5 class="col-sm-4 col-md-4 col-lg-4 col-xl-4" style="text-align:center;" ><b>{{ $dest->precio }}</b>€</h5>
                            <div class="col-sm-4 col-md-4 col-lg-4 col-xl-4"></div>
                         </div>
                        </a>
                    </div>
                @endforeach  
            </section>
        
    </article>