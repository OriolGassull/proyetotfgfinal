@include('productos.Includes.headers.header-links')

<div class="container">
    <div class="row justify-content-center mt-5">
        <div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">
            <div class="card">
                <div class="row">
                    <form method="POST" action="{{ route('login') }}" class="col-sm-7 col-md-7 col-md-7 col-lg-7 col-xl-7" >
                        @csrf
                        <div class="text-right text-sm-right text-md-right">
                            <a class="nav-link" href="{{ route('register') }}" style=" cursor: pointer;">{{ __('Registrarse') }}</a>
                        </div>    
                        <div class="row"> 
                            <div style="width:20%;"></div>
                            <!--Logo Empresa--> 
                            <div  style="margin-top:13%; width:60%;">
                                <img src="LogoEmrpesa.png" height="auto" class="img-responsive" width="100%">
                            </div>
                            <div style="width:20%;"></div>
                        </div> 
                        <div class="form-group row" style="margin-top:15%;">
                            <div class="col-1 col-sm-2 col-md-2 text-md-center">
                                    
                            </div>

                            <div class="col-10 col-sm-9 col-md-8 col-lg-9 col-xl-9">
                            <label for="email">{{ __('Correo') }}</label>
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-1 col-sm-2 col-md-2 text-md-center">
                                
                            </div>

                            <div class="col-10 col-sm-9 col-md-8 col-lg-9 col-xl-9">
                                <label for="password">{{ __('Contraseña') }}</label>
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6  offset-1 offset-sm-2 offset-md-2">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Recuerdame') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-11 offset-1 col-md-12 offset-sm-1 offset-md-2">
                                <button type="submit" class="btn btn-dark">
                                    {{ __('Iniciar Sesión') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link col-6 col-sm-6" href="{{ route('password.request') }}">
                                        {{ __('Contraseña olvidada') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                        
                    </form>
                    <div class="col-sm-5 col-md-5 col-lg-5 col-xl-5 d-none d-sm-block">
                        <img src="http://getwallpapers.com/wallpaper/full/6/7/b/364618.jpg" height="100%" width="100%">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
