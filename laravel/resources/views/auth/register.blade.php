
<link rel="stylesheet" href="{{ asset('css/style.css') }}"/>

@include('productos.Includes.headers.header-links')

<div class="container" style="margin-top:3%">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="row">
                    <div class="col-sm-5 col-md-5 col-lg-5 col-xl-5 d-none d-sm-block">
                        <img src="http://getwallpapers.com/wallpaper/full/6/7/b/364618.jpg" height="100%" width="100%">
                    </div>
                    <form method="POST" action="{{ route('register') }}" class="col-sm-7 col-md-7 col-md-7 col-lg-7 col-xl-7">
                        @csrf

                        <div class="text-left text-sm-left text-md-left">
                           
                            <a class="nav-link" href="{{ route('login') }}" style=" cursor: pointer;">{{ __('Iniciar Sesion') }}</a>
                        
                        </div> 

                        <!--Logo Empresa--> 
                        <div class="row"> 

                            <div style="width:20%;"></div>
                           
                            <div  style="margin-top:13%; width:60%;">
                                <img src="LogoEmrpesa.png" height="auto" class="img-responsive" width="100%">
                            </div>

                            <div style="width:20%;"></div>

                        </div>

                        <!--Primera linea-->
                        
                        <div class="form-group row" style="margin-top:15%;">
                        
                        <div class="col-1 col-sm-1 col-md-1 text-md-center"></div>

                            <!--Nombre-->
                            <div class="col-md-3">

                                <label for="name">{{ __('Nombre') }}</label>
                                
                                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                             <!--apellido 1-->
                            <div class="col-md-3">

                                <label for="primerapellido">{{ __('Primer apellido') }}</label>
                                
                                    <input id="primerapellido" type="text" class="form-control @error('primerapellido') is-invalid @enderror" name="primerapellido" value="{{ old('primerapellido') }}" required autocomplete="primerapellido" autofocus>

                                @error('primerapellido')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <!--apellido 2-->
                            <div class="col-md-3">

                                <label for="segundoapellido">{{ __('Segundo Apellido') }}</label>
                                
                                    <input id="segundoapellido" type="text" class="form-control @error('segundoapellido') is-invalid @enderror" name="segundoapellido" value="{{ old('segundoapellido') }}" required autocomplete="segundoapellido" autofocus>

                                @error('segundoapellido')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        
                        <!--Segunda linea-->
                        <div class="form-group row">
                        
                        <div class="col-1 col-sm-1 col-md-1 text-md-center"></div>

                        <!--Correo-->
                        <div class="col-md-6">

                            <label for="email">{{ __('Correo') }}</label>

                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <!--Nacimiento-->
                            <div class="col-md-3">

                                <label for="nacimiento">{{ __('Nacimiento') }}</label>
                                
                                    <input id="nacimiento" type="date" class="form-control @error('nacimiento') is-invalid @enderror" name="nacimiento" value="{{ old('nacimiento') }}" required autocomplete="nacimiento" autofocus>

                                @error('nanacimientome')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">

                            <div class="col-1 col-sm-1 col-md-1 text-md-center"></div>

                            <div class="col-md-4">

                                <label for="password">{{ __('Contraseña') }}</label>

                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-1 col-sm-1 col-md-1 text-md-center"></div>

                            <div class="col-md-4">

                                <label for="password-confirm" >{{ __('Confirmar contraseña') }}</label>

                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-7 col-sm-7 col-md-7 offset-7 offset-sm-7 offset-md-7 mt-3">
                                <button type="submit" class="btn btn-dark">
                                    {{ __('Registrarse') }}
                                </button>
                            </div>
                        </div>
                    </form>
                    </div>
            </div>
        </div>
    </div>
</div>
