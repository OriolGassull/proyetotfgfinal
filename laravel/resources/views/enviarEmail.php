<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

// Load Composer's autoloader
require '/home/oriol/proyectoTFG/laravel/vendor/autoload.php';

// Instantiation and passing `true` enables exceptions
$mail = new PHPMailer;

$mail->isSMTP();                                      // Set mailer to use SMTP
$mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
$mail->SMTPAuth = true;                               // Enable SMTP authentication
$mail->Username = 'soporteelectromes@gmail.com';                 // SMTP username
$mail->Password = 'Electromes123';                           // SMTP password
$mail->SMTPSecure = 'tls';      // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
$mail->Port = 587;         // Enable encryption, 'ssl' also accepted

$mail->From = 'depuracion@audinforsystem.es';
$mail->FromName = 'Depuración';
$mail->addAddress('oriol.gassul@audinforsystem.es', 'Oriol Gassull');     // Add a recipient

$mail->Subject = 'Mensaje de prueba';
$mail->Body    = 'This is the HTML message body <b>in bold!</b>';
//$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

if(!$mail->send()) {
    echo 'Message could not be sent.';
    echo 'Mailer Error: ' . $mail->ErrorInfo;
} else {
    echo 'Message has been sent';
}
?>