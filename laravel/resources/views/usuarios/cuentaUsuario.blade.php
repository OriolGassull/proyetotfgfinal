@include('productos.Includes.headers.header-links')
<!--Logo Empresa-->         
<div>
            <a href="/productos"><img src="../LogoEmrpesa.png" id="imagenEmpresa" 
            style="cursor: pointer; width: 130px; height: 40px; margin-top: 12px; margin-right: 15px;"></a>
        </div>
@include('productos.Includes.headers.header')
 <!--Mapa de Navegación-->
        <nav>
            <div class="row">
                <div id="mapaNavegacion" class="col-12 col-sm-7 col-md-5 col-lg-5 col-xl-5"><a href="/productos">Inicio</a> > Mi cuenta</div>
                <div id="textoPagina" class="d-none d-sm-block d-md-block d-lg-block col-sm-5 col-md-7 col-lg-7 col-xl-7"></div>
            </div>
        </nav>
        <script>
            $(document).ready(function(){
                $("#DU").click(function(){
                    $("#contenidoUsuario").show();
                    $("#articlePedidos").hide();
                    $("#articleResenas").hide();
                    $("#articleFavoritos").hide();
                });

                $("#R").click(function(){
                    $("#articleResenas").show();
                    $("#articlePedidos").hide();
                    $("#contenidoUsuario").hide();
                    $("#articleFavoritos").hide();
                });

                $("#PF").click(function(){
                    $("#articleFavoritos").show();
                    $("#articlePedidos").hide();
                    $("#articleResenas").hide();
                    $("#contenidoUsuario").hide();
                });
                
                $("#HP").click(function(){
                    $("#articlePedidos").show();
                    $("#articleFavoritos").hide();
                    $("#articleResenas").hide();
                    $("#contenidoUsuario").hide();
                });
                $("#fotoUsuario").click(function(){
                    $("#editarFotoUser").show();
                });
                $("#cerrarFotoUser").click(function(){
                    $("#editarFotoUser").hide();
                });
                
            });
        </script>
<div class="container-fluid">
    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12" id="usuarioDatos" >  
        <div class="row"> 
            <aside class="col-12 col-sm-12 col-md-12 col-lg-2 col-xl-2" id="asideUsuario">
                <div class="recuadrosUsuario" id="DU">
                    <p>Datos de Usuario</p>
                </div>

                <div class="recuadrosUsuario"  id="HP">
                    <p>Historial de pedidos</p>
                </div>
                
                <div class="recuadrosUsuario"  id="PF">
                    <p>Productos favoritos <i class="fas fa-heart"></i></p>
                </div>

                <div class="recuadrosUsuario" id="R">
                    <p>Reseñas</p>
                </div>

            </aside>

            <!--Detalles de usuario-->

            <article id="contenidoUsuario" class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10 pb-3">
                <section class="form-group" >
                <!--Header usuario-->
                    <section id="headerUsuario">
                    <form action="{{ route('detalles-usuario.update', 'fotoUser' ) }}" method="POST" role="form" enctype="multipart/form-data">

                        {{ csrf_field() }}

                        @method('PUT')

                        <div id="editarFotoUser" style="display:none">
                            <input type="file" name="imagenPerfil">
                            <i class="ml-3 fa fa-close" id="cerrarFotoUser"></i>
                            <button type="sumbit" style="background-color:rgb(0,0,0,0);border:solid 1px rgb(0,0,0,0)"><i class="ml-3 fa fa-check" id="cerrarFotoUser"></i></button>
                        </div>
                    
                        <img src="../usuario/fotoPerfil/{{ Auth::user()->id }}/{{ Auth::user()->imagenPerfil }}" id="fotoUsuario" class="imagenesRes">

                    </form>
                        <h4>Bienvenid@, {{ Auth::user()->name }}</h4>
                    </section>

                <!--Datos de usuario-->
                <form action="{{ route('detalles-usuario.update', 'datos' ) }}" method="POST">

                    @csrf

                    @method('PUT')
                    <section style="margin-top:200px;">
                        <strong>Datos de usuario</strong>
                        <div class="row mt-2">   
                            <div class="col">
                                <input type="text" name="name" class="form-control" value="{{ Auth::user()->name }}" placeholder="Nombre">
                            </div>  

                            <div class="col">  
                                <input type="text" name="primerapellido" class="form-control" value="{{ Auth::user()->primerapellido }}" placeholder="Primer Apellido">
                            </div>

                            <div class="col">  
                                <input type="text" name="segundoapellido" class="form-control" value="{{ Auth::user()->segundoapellido }}" placeholder="Segundo Apellido">
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-md-6">  
                                <input type="text" name="email" class="form-control" value="{{ Auth::user()->email }}" placeholder="Correo electronico">
                            </div>

                            <div class="col-md-3">  
                                <input type="date" name="nacimiento" class="form-control" value="{{ Auth::user()->nacimiento }}" placeholder="Fecha de nacimiento *" required>
                            </div>
                            <div class="col-md-3"> 
                                <input type="tel" name="telefono" maxlength="9" class="form-control" value="{{ Auth::user()->telefono }}" placeholder="Telefono *" pattern="[6]{1}[0-9]{8}" required>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-dark float-right mt-3">Guardar</button>
                    </section>
                </form>


            <form action="{{ route('detalles-usuario.update', 'datos' ) }}" method="POST">

                @csrf

                @method('PUT')
                <!--Datos de Enviío-->
                    <section class="mt-5">
                    <strong>Datos de envío</strong>
                        <div class="row mt-2">
                            <div class="col">   
                                <input type="text" name="calle" maxlength="40" class="form-control" value="{{ Auth::user()->calle }}" placeholder="Calle">
                            </div>    
                            <div class="col-2">
                                <input type="text" name="numeroCalle" maxlength="5" class="form-control" value="{{ Auth::user()->numeroCalle }}" placeholder="Numero" >
                            </div>  
                            <div class="col-2">  
                                <input type="text" name="letraCalle"  maxlength="3" class="form-control" value="{{ Auth::user()->letraCalle }}" placeholder="Letra">
                            </div>  
                        </div>
                        <div class="row mt-3">
                            <div class="col">  
                                <input type="text" name="ciudad"  maxlength="30" class="form-control" value="{{ Auth::user()->ciudad }}" placeholder="Ciudad">
                            </div>
                            <div class="col">  
                                <input type="text" name="provincia"  maxlength="30" class="form-control" value="{{ Auth::user()->provincia }}" placeholder="Provincia">
                            </div>
                            <div class="col">  
                                <input type="text" name="codigoPostal"  maxlength="5" class="form-control" value="{{ Auth::user()->codigoPostal }}" placeholder="codigopostal">
                            </div>
                        </div>
                        <button type="submit" class="btn btn-dark float-right mt-3">Guardar</button>
                    </section>
                </form>
                
                <form action="{{ route('detalles-usuario.update', 'contra' ) }}" method="POST">

                @csrf

                @method('PUT')
                 <!--Productos Favoritos-->
                    <section class="mt-5">
                    <strong>Contraseña</strong>
                        <div class="row mt-2">
                            <div class="col">   
                                <input type="password" name="password" class="form-control" placeholder="Contraseña">
                            </div>  
                            <div class="col">   
                                <input type="password" name="password_confirmation" class="form-control" placeholder="Repetir contraseña">
                            </div>   
                        </div>
                        <button type="submit" class="btn btn-dark float-right mt-3">Guardar</button>
                    </section>
                 </form>
                </section>
            </article>

            <!--Reseñas-->

            <article id="articleResenas" class="col-12 col-sm-12 col-md-12 col-lg-9 col-xl-10 pt-4" style="display:none">
                @foreach ($resenas as $resena)
                    <section class="pb-4">
                        <div class="row">
                            <div class="col-2 col-sm-2 col-md-2 col-lg-1 col-xl-1 ml-2">
                                <img src="../usuario/fotoPerfil/{{ Auth::user()->id }}/{{ Auth::user()->imagenPerfil }}" id="imagenUsuarioResena">
                            </div>
                     
                            <div class="eleiminarBotResen">
                                <form action="{{ route('resenas.destroy',$resena->id) }}" method="POST">
                                    @csrf @method('DELETE')
                                    <button type="submit" id="botonEliminarCarrito"><h3><i class="fa fa-close pt-3"></i></h3></button>
                                </form>
                            </div>
                            <div class="col-9 col-sm-9 col-md-9 col-lg-10 col-xl-10">
                                <textarea name="descripcion" id="inputResena" disabled>{{$resena->descripcion}}</textarea>
                            </div>
                    
                        </div>
                        <p id="nombreResena"><a href="{{ route('productos.show',$resena->id_producto) }}">Producto</a><p>
                            
                        <b class="rating2 ml-4 pr-1">
                            @for ($i = 0; $i < $resena->valoracion ; $i++)   
                                    <i class="fa fa-star puntuacionReseñas"></i>
                            @endfor
                        </b>                    
                    </section>
                    <hr>
                @endforeach
            </article>

            <!--Productos Favorito-->
            
            <article id="articleFavoritos" class="col-12 col-sm-12 col-md-12 col-lg-9 col-xl-10 pt-4"  style="display:none">
            @for ($i = 0; $i < $numFavoritos ; $i++)   
            
                <section id="productosFavortios">
                    <a href="{{ route('productos.show',$favorito[$i]->id_producto) }}">

                        <img src="../producto/{{ $favortioProductos[$i]['categoria']}}/{{ $favortioProductos[$i]['id']}}/{{ $favortioProductos[$i]['imagen1']}}" class="imagenesRes" >
                        
                        <p style="text-align:center;white-space: nowrap; overflow: hidden;" >{{ $favorito[$i]->nombreProducto }} </p>

                        <div class="row">
                            <div class="col-sm-4 col-md-4 col-lg-4 col-xl-4"></div>
                                <h5 class="col-sm-4 col-md-4 col-lg-4 col-xl-4 text-center"><b>{{ $favorito[$i]->precioProducto }}</b>€</h5>
                            <div class="col-sm-4 col-md-4 col-lg-4 col-xl-4"></div>
                        </div>

                    </a>
                </section>
            @endfor
            </article>               
            
             <!--Productos pedido-->
             
             <article id="articlePedidos" class="col-12 col-sm-12 col-md-12 col-lg-9 col-xl-10 pt-4" style="display:none">
             @for ($i = 0; $i < $numPedidos ; $i++)
                   <div class="row ml-2">
                        Número pedido: <b class="mr-3"> {{$pedido[$i]['numero_pedido']}} </b>
                        Fecha envío:  <b class="mr-3"> {{$pedido[$i]['fechaEnvio']}} </b> 
                        Fecha de llegada:  <b> {{$pedido[$i]['fechaLlegada']}} </b>
                    </div>     
                <div class="row" style="border-bottom:solid 1px black"> 
                    <section class="col-4 col-sm-4 col-md-4 col-lg-4 col-xl-3">
                        
                        <a href="{{ route('productos.show',$pedido[$i]['id_producto']) }}">
                            <img src="../producto/{{ $pedidoProductos[$i]['categoria']}}/{{ $pedidoProductos[$i]['id']}}/{{ $pedidoProductos[$i]['imagen1']}}" class="imagenesRes">
                        
                    </section>
                    <section class="col-8 col-sm-8 col-md-8 col-lg-8 col-xl-9">
                        <h4 style="white-space: nowrap; overflow: hidden; margin-top:30px;">{{$pedidoProductos[$i]['nombre']}} </h4>
                        </a>
                        <h5 class="col-sm-4 col-md-4 col-lg-4 col-xl-4 mt-5"><b>Cantidad: </b>{{$pedido[$i]['cantidad']}}</h5>
                        <div class="row float-right">
                            <button disabled style="height:30px;" class="mt-5 mr-3">Factura</button>
                            <h3 class="mt-5 mr-4"><b>{{$pedido[$i]['precioTotal']}} </b>€</h3>
                        </div>
                        
                    </section>      
                </div>
            @endfor
            </article>
        </div>
    </div>
</div>

@include('productos.Includes.footers.footer')

