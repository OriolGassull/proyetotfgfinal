<?php

  

use Illuminate\Database\Migrations\Migration;

use Illuminate\Database\Schema\Blueprint;

use Illuminate\Support\Facades\Schema;

  

class CreateProductosTable extends Migration

{

    /**

     * Run the migrations.

     *

     * @return void

     */

    public function up()

    {

        Schema::create('productos', function (Blueprint $table) {

            $table->bigIncrements('id');

            $table->string('nombre');

            $table->string('descripcion');

            $table->integer('precio');
            
            $table->string('categoria');
            
            $table->string('marca');

            $table->string('memoria')->nullable();

            $table->string('almacenamiento')->nullable();

            $table->string('bateria')->nullable();

            $table->string('pulgadas')->nullable();

            $table->string('procesador')->nullable();

            $table->string('tarjetaGrafica')->nullable();

            $table->string('conectividad')->nullable();

            $table->string('tipoPantalla')->nullable();

            $table->string('tipoImpresion')->nullable();

            $table->string('altura')->nullable();
            
            $table->string('otros1')->nullable();

            $table->string('otros2')->nullable();

            $table->string('otros3')->nullable();

            $table->string('anchura')->nullable();

            $table->string('imagen1')->nullable();

            $table->string('imagen2')->nullable();

            $table->string('imagen3')->nullable();

            $table->string('imagen4')->nullable();

            $table->string('imagen5')->nullable();

            $table->string('valoracion')->nullable();

            $table->integer('vendidos')->nullable();

            $table->timestamps();

        });

    }

  

    /**

     * Reverse the migrations.

     *

     * @return void

     */

    public function down()

    {

        Schema::dropIfExists('productos');

    }

}

