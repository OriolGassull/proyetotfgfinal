<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePedidosTable extends Migration

{

    /**

     * Run the migrations.

     *

     * @return void

     */

    public function up()
    {
        Schema::create('pedidos', function (Blueprint $table) {

            $table->string('id_pedido');

            $table->text('id_cliente');

            $table->text('id_producto');

            $table->date('fechaEnvio')->nullable();

            $table->date('fechaLlegada')->nullable();

            $table->integer('numero_pedido');

            $table->integer('precioTotal')->nullable();
            
            $table->integer('iva','21');

            $table->integer('canitdad')->nullable();

            $table->string('calle');

            $table->integer('numero_direccion')->nullable();

            $table->string('letra_direccion')->nullable();

            $table->string('ciudad');

            $table->integer('codigo_postal')->nullable();

            $table->string('municipio')->nullable();

            $table->string('pais');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pedidos');
    }
}
