<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {

            $table->bigIncrements('id');

            $table->string('name');

            $table->string('imagenPerfil')->nullable();

            $table->string('primerapellido');

            $table->string('segundoapellido');

            $table->string('email')->unique();

            $table->timestamp('email_verified_at')->nullable();

            $table->date('nacimiento');

            $table->integer('telefono')->nullable();

            $table->string('calle')->nullable();

            $table->string('numeroCalle')->nullable();

            $table->string('letraCalle')->nullable();

            $table->string('ciudad')->nullable();

            $table->string('provincia')->nullable();

            $table->string('codigoPostal')->nullable();

            $table->string('password');

            $table->string('esAdmin')->default('false');

            $table->rememberToken();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}

