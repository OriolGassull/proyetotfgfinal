<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ControllerProducto;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('productos', App\Http\Controllers\ControllerProducto::class);

Route::resource('detalles-usuario', App\Http\Controllers\HomeController::class)->middleware('auth');

Route::resource('carrito', App\Http\Controllers\ControllerCarrito::class);

Route::resource('favorito', App\Http\Controllers\ControllerFavorito::class);

Route::resource('resenas', App\Http\Controllers\ControllerResena::class);

Route::resource('pago', App\Http\Controllers\ControllerPedido::class);

Route::get('/ajax', 'App\Http\Controllers\ControllerProducto@ajax')->name('ajax');

Route::get('/ofertas', 'App\Http\Controllers\ControllerProducto@ofertas');

Route::get('/soporte', 'App\Http\Controllers\ControllerProducto@soporte');

Route::get('/destacados', 'App\Http\Controllers\ControllerProducto@destacados');

Route::get('/recomendados', 'App\Http\Controllers\ControllerProducto@recomendados');

Route::post('/sendmail',[
    'as' => 'sendmail',
    'uses' => 'App\Http\Controllers\ControllerPhpmailer@sendEmail'
  ]);

Auth::routes();

Route::resource('categoria', App\Http\Controllers\ControllerCategoria::class);


/*Route::group(['prefix' => 'electromes'], function(){
    
});

*/