<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pedidos extends Model
{
    use HasFactory;

    protected $fillable = [

        'id_cliente','id_producto','fechaEnvio', 'fechaLlegada', 'numero_pedido', 'calle', 'numero_direccion', 
        'letra_direccion', 'ciudad', 'codigo_postal', 'provincia', 'pais','precioTotal','iva','cantidad'

    ];

}