<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Resenas extends Model
{
    use HasFactory;

    protected $fillable = [

        'id_producto', 'id_cliente', 'nombre','descripcion', 'valoracion'

    ];
}
