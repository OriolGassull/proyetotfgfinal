<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

use Illuminate\Database\Eloquent\Model;

  

class Producto extends Model

{

    use HasFactory;

    protected $fillable = [

        'nombre', 'descripcion', 'precio', 'categoria', 'marca', 'memoria',
        'almacenamiento', 'bateria', 'pulgadas','procesador', 'tarjetaGrafica',
        'conectividad', 'tipoPantalla', 'tipoImpresion', 'anchura', 'altura', 
        'otros1', 'otros2', 'otros3', 'imagen1', 'imagen2', 'imagen3', 'imagen4', 
        'imagen5','valoracion', 'vendidos','esAdmin'

    ];

}
