<?php

namespace App\Http\Controllers;

use App\Models\Producto;
use App\Models\Pedidos;
use App\Models\Carrito;
use Response;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ControllerPedido extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $carritoUs =  Carrito::where('id_cliente', $request['id_cliente'])->get();

        $numero_pedido = Pedidos::orderBy('numero_pedido', 'desc')->latest()->value('numero_pedido'); 

        $request['numero_pedido'] = $numero_pedido + 1;

        for($i=0;$i<sizeof($carritoUs);$i++){       
            $array[$i] = [
                "id_producto" => $carritoUs[$i]['id_producto'],
                "cantidad" => $carritoUs[$i]['cantidad'],
            ];
            $request['id_producto'] = $array[$i]['id_producto'];
            $request['cantidad'] = $array[$i]['cantidad'];

            Pedidos::create($request->all());
        }

        $vendidos = 0;

        $nvend = Producto::where('id',  $request['id_producto'])->value('vendidos');
       
        $vendidos = $nvend + $request['cantidad'];

        Producto::where('id',  $request['id_producto'])->update(['vendidos' => $vendidos]);

        Carrito::where('id_cliente', $request['id_cliente'])->delete();

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
