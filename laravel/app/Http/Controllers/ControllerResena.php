<?php

namespace App\Http\Controllers;

use App\Models\Resenas;

use Illuminate\Http\Request;

class ControllerResena extends Controller
{
      /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('productos.productoVentana');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
        
        $request->validate([

        'id_producto' => 'required',

        'id_cliente' => 'required',

        'descripcion' => 'required',

        'valoracion' => 'required',

    ]);

        Resenas::create($request->all());
        
        return back();
    }
   
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

         
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $request->validate([

            'id_producto' => 'required',

            'id_cliente' => 'required',

            'nombre' => 'required',

            'descripcion' => 'required',

            'valoracion' => 'required',

        ]);

        $resena = Resenas::where('id', $id)->first();

        $resena->update($request->all());
 
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        Resenas::where('id', $id)->delete(); 

        return back();
    }
}
