<?php

namespace App\Http\Controllers;

use App\Models\Carrito;
use App\Models\Producto;
use Response;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ControllerCarrito extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('productos.carrito');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
        
        $request->validate([

        'id_producto' => 'required',

        'id_cliente' => 'required',

        'nombreProducto' => 'required',

        'precioProducto' => 'required',

        'cantidad' => 'required'  

    ]);

        Carrito::create($request->all());
        
        return back();
    }
   
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $numCarr = Carrito::where('id_cliente', $id)->count();

        $datos = Carrito::where('id_cliente', $id)->get(); 

        if($numCarr != 0){
            for($i=0;$i<sizeof($datos);$i++){

                $datosProducto[$i] = [
                    "id" => Producto::where('id', $datos[$i]['id_producto'])->value('id') ,
                    "categoria" => Producto::where('id', $datos[$i]['id_producto'])->value('categoria'),
                    "imagen1" => Producto::where('id', $datos[$i]['id_producto'])->value('imagen1'),
                ];
            } 
        }else{
            $datosProducto = 0; 
        }
        

  

        $precioTotal=0;

        for($i=0;$i<$numCarr;$i++){
            $add = $datos[$i]->precioProducto;

            $precioTotal = ($add * $datos[$i]->cantidad) + $precioTotal ;
        }
        

        return view('productos.carrito', compact('datos','numCarr','precioTotal','datosProducto'));

         
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $request->validate([

            'id_producto' => 'required',

            'nombreProducto' => 'required',

            'id_cliente' => 'required',

            'precioProducto' => 'required',

            'cantidad' => 'required'

        ]);

        $producto = Carrito::where('id', $id)->first();

        $producto->update($request->all());
 
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        Carrito::where('id_producto', $id)->delete(); 

        return back();
    }
}
