<?php

namespace App\Http\Controllers;

use App\Models\Producto;
use App\Models\Carrito;
use App\Models\Resenas;
use App\Models\Favorito;   
use App\Models\User;
use Yajra\Datatables\Datatables;

use Auth;

use Response;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

  

class ControllerProducto extends Controller

{

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function index()

    {

        $destacados = Producto::orderBy('valoracion', 'desc')->latest()->paginate(4); 
        
        $recomendaciones = Producto::orderBy('vendidos', 'desc')->latest()->paginate(4); 
        
        return view('productos.principal',compact('destacados','recomendaciones'));

    }


    

    public function ajax()
    {
        $destacados = Producto::orderBy('valoracion', 'desc')->latest()->paginate(4); 
        
        $recomendaciones = Producto::orderBy('vendidos', 'desc')->latest()->paginate(4); 
        
        return view('productos.principal',compact('destacados','recomendaciones'));
    }

    public function destacados()
    {
        $verTodas = Producto::orderBy('valoracion', 'desc')->get(); 
        
        return view('productos.verTodas',compact('verTodas'));
    }

    public function recomendados()
    {
        $verTodas = Producto::orderBy('vendidos', 'desc')->get(); 
        
        return view('productos.verTodas',compact('verTodas'));
    }
    
    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function tablaProductos()
    {
       return Datatables::of(Producto::query())->make(true);
    }

    public function ofertas()
    {
        $ofertas = Producto::get(); 

        return view('productos.ofertas',compact('ofertas'));
    }

    public function soporte()
    {
        return view('productos.soporte');
    }
    
    

    public function categoriaProducto($categoria)

    {
        //Cogemos todos los datos de los productos que sean de misma $categoria
        $categorias = Producto::where('categoria', $categoria)->orderBy('valoracion','desc')->get();

        //Nombre de la categoria
        $categoriasName = Producto::where('categoria', $categoria)->value('categoria');

        $productoVendido = Producto::where('categoria', $categoria)->orderBy('vendidos','desc')->get();

        //Numero de productos en $categoria
        $numCategoria = Producto::where('categoria', $categoria)->count();

        $productoAsc = Producto::where('categoria', $categoria)->orderBy('precio','asc')->get();

        $productoDesc = Producto::where('categoria', $categoria)->orderBy('precio','desc')->get();

        return view('productos.productoCategoria')->with(compact('categorias','categoriasName','productoVendido','numCategoria','productoAsc','productoDesc'));
    }
    
    /**

     * Show the form for creating a new resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function create()

    {

        return view('productos.CRUD.create');

    }

    

    /**

     * Store a newly created resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     */

    public function store(Request $request)

    {
        $idProducto = Producto::orderBy('id', 'desc')->value('id'); 

        $idProducto =  $idProducto + 1;

        $prod = new Producto();

        $prod->nombre = $request->input('nombre'); $prod->categoria = $request->input('categoria'); $prod->precio = $request->input('precio');
        $prod->marca = $request->input('marca'); $prod->descripcion = $request->input('descripcion'); $prod->memoria = $request->input('memoria');
        $prod->almacenamiento = $request->input('almacenamiento'); $prod->bateria = $request->input('bateria'); $prod->pulgadas = $request->input('pulgadas');
        $prod->procesador = $request->input('procesador'); $prod->tarjetaGrafica = $request->input('tarjetaGrafica'); $prod->conectividad = $request->input('conectividad');
        $prod->tipoPantalla = $request->input('tipoPantalla'); $prod->tipoImpresion = $request->input('tipoImpresion'); $prod->altura = $request->input('altura');
        $prod->anchura = $request->input('anchura'); $prod->otros1 = $request->input('otros1'); $prod->otros2 = $request->input('otros2');
        $prod->otros3 = $request->input('otros3');

        $this->validate($request, [
            'imagen1' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:1024',
            'imagen2' => 'image|mimes:jpeg,png,jpg,gif,svg|max:1024',
            'imagen3' => 'image|mimes:jpeg,png,jpg,gif,svg|max:1024',
            'imagen4' => 'image|mimes:jpeg,png,jpg,gif,svg|max:1024',
            'imagen5' => 'image|mimes:jpeg,png,jpg,gif,svg|max:1024',
            'nombre' => 'required',
            'categoria' => 'required',
            'precio' => 'required',
            'marca' => 'required',
            'descripcion' => 'required'
        ]);

        $cantidadImagenes = 0;

        if($request->hasfile('imagen1')){$cantidadImagenes = $cantidadImagenes + 1;}
        if($request->hasfile('imagen2')){$cantidadImagenes = $cantidadImagenes + 1;}
        if($request->hasfile('imagen3')){$cantidadImagenes = $cantidadImagenes + 1;}
        if($request->hasfile('imagen4')){$cantidadImagenes = $cantidadImagenes + 1;}
        if($request->hasfile('imagen5')){$cantidadImagenes = $cantidadImagenes + 1;}

        for($i=1;$i<=$cantidadImagenes;$i++){
            
            $image[$i] = $request->file('imagen' . $i); // Coge la imagen 

            $name[$i] = time(). $i .'.'.$image[$i]->getClientOriginalExtension(); // Le da un nombre a la imagen con la extension  

            $destinationPath = public_path('/producto' . "/" . $request["categoria"] . "/" . $idProducto); // Crea una carpeta en public

            if ($i == 1) {$prod->imagen1 = $name[$i];
                $image[$i]->move($destinationPath, $name[$i]);} // Mueve la imagen a la carpeta //Le doy nombre al archivo  

            if ($i == 2) {$prod->imagen2 = $name[$i];
                $image[$i]->move($destinationPath, $name[$i]);} // Mueve la imagen a la carpeta} //Le doy nombre al archivo  

            if ($i == 3) {$prod->imagen3 = $name[$i];
                $image[$i]->move($destinationPath, $name[$i]);} // Mueve la imagen a la carpeta} //Le doy nombre al archivo  

            if ($i == 4) {$prod->imagen4 = $name[$i];
                $image[$i]->move($destinationPath, $name[$i]);} // Mueve la imagen a la carpeta} //Le doy nombre al archivo  

            if ($i == 5) {$prod->imagen5 = $name[$i];
                $image[$i]->move($destinationPath, $name[$i]);} // Mueve la imagen a la carpeta} //Le doy nombre al archivo  
        }

       $prod->save();
        
        $productos = Producto::latest()->paginate(4);

        $destacados = Producto::latest()->paginate(4); 
        
        $recomendaciones = Producto::orderBy('vendidos', 'desc')->latest()->paginate(4);  

        return view('productos.principal',compact('productos','destacados','recomendaciones'));

    }

    

    /**

     * Display the specified resource.

     *

     * @param  \App\Producto  $product

     * @return \Illuminate\Http\Response

     */

    public function show(Producto $producto)

    {
        $id_clienteResena = 0;
        
        $destacados = Producto::latest()->paginate(4); 

        $id_clienteCarrito = Carrito::where('id_producto',  $producto['id'])->value('id_cliente');
        
        $id_clienteFavorito = Favorito::where('id_producto',  $producto['id'])->value('id_cliente');

        if(Auth::check()){

            $id_clienteResena = Resenas::where('id_producto',  $producto['id'])->where('id_cliente', auth()->user()->id)->value('id_cliente');

        }

        $resenas = Resenas::where('id_producto', $producto['id'])->get();  

        $ayuda = Resenas::where('id_producto', $producto['id'])->get(); 

        $nRes = Resenas::where('id_producto', $producto['id'])->count();


        if($nRes != 0){
            for($k=0;$k<$nRes;$k++){
                $imagenUser[$k] = [
                    "imagenPerfil" => User::where('id', $resenas[$k]['id_cliente'])->value('imagenPerfil'),
                ];
            }
        }else{
            $imagenUser = 0;
        }
        $mediaValoracion = 0;

        /*Sacar media de valoraciones*/
        if($nRes == 0){
        }
        else{
            
            $ala =  0;
    
            for($i=0;$i<$nRes;$i++){
                $ala = $ala + $ayuda[$i]->valoracion;     
            }
            
            $ala =  $ala / $nRes;
         
            $mediaValoracion = round($ala);
            
        }

        Producto::where('id',  $producto['id'])->update(['valoracion' => $mediaValoracion]);

        return view('productos.productoVentana',compact('producto','destacados','id_clienteCarrito','id_clienteFavorito','id_clienteResena','resenas','nRes','mediaValoracion','imagenUser'));

    } 


    /**

     * Show the form for editing the specified resource.

     *

     * @param  \App\Producto  $product

     * @return \Illuminate\Http\Response

     */

    public function edit(Producto $producto)

    {

        return view('productos.CRUD.edit',compact('producto'));

    }

    

    /**

     * Update the specified resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  \App\Producto  $product

     * @return \Illuminate\Http\Response

     */

    public function update(Request $request,$id)

    {

        $prod = Producto::find($id);

        $prod->nombre = $request->input('nombre'); $prod->categoria = $request->input('categoria'); $prod->precio = $request->input('precio');
        $prod->marca = $request->input('marca'); $prod->descripcion = $request->input('descripcion'); $prod->memoria = $request->input('memoria');
        $prod->almacenamiento = $request->input('almacenamiento'); $prod->bateria = $request->input('bateria'); $prod->pulgadas = $request->input('pulgadas');
        $prod->procesador = $request->input('procesador'); $prod->tarjetaGrafica = $request->input('tarjetaGrafica'); $prod->conectividad = $request->input('conectividad');
        $prod->tipoPantalla = $request->input('tipoPantalla'); $prod->tipoImpresion = $request->input('tipoImpresion'); $prod->altura = $request->input('altura');
        $prod->anchura = $request->input('anchura'); $prod->otros1 = $request->input('otros1'); $prod->otros2 = $request->input('otros2');
        $prod->otros3 = $request->input('otros3');

        $this->validate($request, [
            'imagen1' => 'image|mimes:jpeg,png,jpg,gif,svg|max:1024',
            'imagen2' => 'image|mimes:jpeg,png,jpg,gif,svg|max:1024',
            'imagen3' => 'image|mimes:jpeg,png,jpg,gif,svg|max:1024',
            'imagen4' => 'image|mimes:jpeg,png,jpg,gif,svg|max:1024',
            'imagen5' => 'image|mimes:jpeg,png,jpg,gif,svg|max:1024',
            'nombre' => 'required',
            'categoria' => 'required',
            'precio' => 'required',
            'marca' => 'required',
            'descripcion' => 'required'
        ]);
        
        $cantidadImagenes = 1;

        if($request->hasfile('imagen2')){$cantidadImagenes = $cantidadImagenes + 1;}
        if($request->hasfile('imagen3')){$cantidadImagenes = $cantidadImagenes + 1;}
        if($request->hasfile('imagen4')){$cantidadImagenes = $cantidadImagenes + 1;}
        if($request->hasfile('imagen5')){$cantidadImagenes = $cantidadImagenes + 1;}

    if($request->hasfile('imagen1')){
        for($i=1;$i<=$cantidadImagenes;$i++){
            
            $image[$i] = $request->file('imagen' . $i); // Coge la imagen 

            $name[$i] = time(). $i .'.'.$image[$i]->getClientOriginalExtension(); // Le da un nombre a la imagen con la extension  

            $destinationPath = public_path('/producto' . "/" . $request["categoria"] . "/" . $id); // Crea una carpeta en public

            if ($i == 1) {$prod->imagen1 = $name[$i];
                $image[$i]->move($destinationPath, $name[$i]);} // Mueve la imagen a la carpeta //Le doy nombre al archivo  

            if ($i == 2) {$prod->imagen2 = $name[$i];
                $image[$i]->move($destinationPath, $name[$i]);} // Mueve la imagen a la carpeta} //Le doy nombre al archivo  

            if ($i == 3) {$prod->imagen3 = $name[$i];
                $image[$i]->move($destinationPath, $name[$i]);} // Mueve la imagen a la carpeta} //Le doy nombre al archivo  

            if ($i == 4) {$prod->imagen4 = $name[$i];
                $image[$i]->move($destinationPath, $name[$i]);} // Mueve la imagen a la carpeta} //Le doy nombre al archivo  

            if ($i == 5) {$prod->imagen5 = $name[$i];
                $image[$i]->move($destinationPath, $name[$i]);} // Mueve la imagen a la carpeta} //Le doy nombre al archivo  
        }
    }
        $prod->save();

        return redirect('productos');

    }
    

    /**

     * Remove the specified resource from storage.

     *

     * @param  \App\Producto  $product

     * @return \Illuminate\Http\Response

     */

    public function destroy(Producto $producto)

    {

        $producto->delete();

        $destacados = Producto::latest()->paginate(4); 
        
        $recomendaciones = Producto::orderBy('vendidos', 'desc')->latest()->paginate(4); 

        return view('productos.principal', compact('destacados','recomendaciones' ));

    }
}