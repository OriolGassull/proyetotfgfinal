<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Producto;
class ControllerCategoria extends Controller
{
    public function show($categoria)
    {
      
        //Cogemos todos los datos de los productos que sean de misma $categoria
        $categorias = Producto::where('categoria', $categoria)->orderBy('valoracion','desc')->get();

        //Nombre de la categoria
        $categoriasName = Producto::where('categoria', $categoria)->value('categoria');

        $productoVendido = Producto::where('categoria', $categoria)->orderBy('vendidos','desc')->get();

        //Numero de productos en $categoria
        $numCategoria = Producto::where('categoria', $categoria)->count();

        $productoAsc = Producto::where('categoria', $categoria)->orderBy('precio','asc')->get();

        $productoDesc = Producto::where('categoria', $categoria)->orderBy('precio','desc')->get();

        return view('productos.productoCategoria')->with(compact('categorias','categoriasName','productoVendido','numCategoria','productoAsc','productoDesc'));
     
    }
}
