<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Resenas;
use App\Models\Producto;
use App\Models\Favorito;
use App\Models\Pedidos;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    { 
        $resenas = Resenas::where('id_cliente', auth()->user()->id)->get(); 

        $numFavoritos = Favorito::where('id_cliente', auth()->user()->id)->count();

        $favorito = Favorito::where('id_cliente', auth()->user()->id)->get(); 

        $numPedidos = Pedidos::where('id_cliente', auth()->user()->id)->count();

        $pedido = Pedidos::where('id_cliente', auth()->user()->id)->get();
        

        for($i=0;$i<sizeof($favorito);$i++){

            $favortioProductos[$i] = [
                "id" => Producto::where('id', $favorito[$i]['id_producto'])->value('id') ,
                "categoria" => Producto::where('id', $favorito[$i]['id_producto'])->value('categoria'),
                "imagen1" => Producto::where('id', $favorito[$i]['id_producto'])->value('imagen1'),
            ];
        }

        if(Pedidos::where('id_cliente', auth()->user()->id)->count() != 0){

            for($i=0;$i<sizeof($pedido);$i++){       
            
                $pedidoProductos[$i] = [
                "nombre" => Producto::where('id', $pedido[$i]['id_producto'])->value('nombre'),
                "id" => Producto::where('id', $pedido[$i]['id_producto'])->value('id') ,
                "categoria" => Producto::where('id', $pedido[$i]['id_producto'])->value('categoria'),
                "imagen1" => Producto::where('id', $pedido[$i]['id_producto'])->value('imagen1'),
            ];
        }
        }else{
            $pedidoProductos = 0; 
            

            //array_push($pedido, $pedidoProductos[$i]['precio']
            
       }


        return view('usuarios.cuentaUsuario',compact('resenas','favorito','pedido','pedidoProductos','favortioProductos','numFavoritos','numPedidos'));
    }

    /**

     * Update the specified resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  \App\User  $product

     * @return \Illuminate\Http\Response

     */

    public function update(Request $request, $param)
    
    {
        if($param == "datos"){

            $user = User::where('id', '=', auth()->user()->id)->first();

            $user->update($request->all());
        }

        if($param == "contra"){

            $user = User::where('id', '=', auth()->user()->id)->first();

            $request['password'] = password_hash($request['password'], PASSWORD_DEFAULT);

            $user->update($request->all());
        }

        if($param == "fotoUser"){
        
            if($request->hasfile('imagenPerfil')){

                $image = $request->file('imagenPerfil'); // Coge la imagen 
        
                $name = time() .'.'.$image->getClientOriginalExtension(); // Le da un nombre a la imagen con la extension  
        
                $destinationPath = public_path('/usuario' . "/" . "fotoPerfil" . "/" . auth()->user()->id); // Crea una carpeta en public
        
                $image->move($destinationPath, $name);
        
                User::where('id',  auth()->user()->id)->update(['imagenPerfil' => $name]);
            }    
        }

        return back();

    }


    public function show(Producto $producto)

    {
        $resenas = Resenas::where('id_producto', $producto['id'])->get(); 

        return view('productos.productoVentana',compact('resenas'));

    } 
}


